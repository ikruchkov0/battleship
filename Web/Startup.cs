﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.Settings;
using Web.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using NSwag;
using NSwag.SwaggerGeneration.Processors.Security;
using Web.Services.Gameplay;
using Web.Hubs;
using Web.Infrastructure.Auth;
using Web.Infrastructure.Validation;
using Web.Services.Gameplay.Implementations;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(OrleansClient.CreateClusterClient);
            services.AddSingleton(s => Mappings.Create());

            services.AddGameplayDependencies();

            services.AddSingleton<PlayerGameValidator>();

            services.AddHostedService<GameListHostedService>();

            services.AddMemoryCache();
            services.AddMvc();

            services.AddSignalR();

            services.AddScoped<TokenProvider>();
            services.AddSettings(Configuration);
            services.AddAuth();

            services.AddSwaggerDocument(document =>
            {
                document.AddSecurity("JWT", Enumerable.Empty<string>(), new SwaggerSecurityScheme
                {
                    Type = SwaggerSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = SwaggerSecurityApiKeyLocation.Header,
                    Description = $"Type into the textbox: {JwtBearerDefaults.AuthenticationScheme} {{your JWT token}}."
                });

                document.OperationProcessors.Add(
                    new AspNetCoreOperationSecurityScopeProcessor("JWT")
                );
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUi3();

            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<GameLobbyHub>("/ws/lobby");
                routes.MapHub<GameHub>("/ws/game");
            });

            app.UseMvc();
        }
    }
}