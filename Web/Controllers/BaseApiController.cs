using System;
using Microsoft.AspNetCore.Mvc;
using Web.Infrastructure.Auth;

namespace Web.Controllers
{
    public abstract class BaseApiController : ControllerBase
    {
        protected Guid PlayerId => GetPlayerId();

        private Guid GetPlayerId()
        {
            return HttpContext.User.Claims.GetPlayerId();
        }
    }


}