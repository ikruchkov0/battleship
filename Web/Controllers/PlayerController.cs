﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Grains.Interfaces.Player;
using Grains.Interfaces.Player.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Orleans;
using Web.Infrastructure.Auth;
using Web.ViewModels;

namespace Web.Controllers
{
    [Authorize]
    [ApiController]
    public class PlayerController : BaseApiController
    {
        private readonly IClusterClient _client;
        private readonly IMapper _mappings;
        private readonly TokenProvider _tokenProvider;

        public PlayerController(IMapper mappings, IClusterClient client, TokenProvider tokenProvider)
        {
            _client = client;
            _mappings = mappings;
            _tokenProvider = tokenProvider;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/player")]
        public async Task<CreatedPlayer> CreatePlayer([FromQuery]string name)
        {
            var id = Guid.NewGuid();
            var grain = _client.GetGrain<IPlayerGrain>(id);
            var status = await grain.CreateAsync(name);
            return _mappings.Map<IPlayerStatus, CreatedPlayer>(status, opt => opt
                .AfterMap((src, dst) => dst.Token = _tokenProvider.GenerateToken(src))
            );
        }

        [HttpGet]
        [Route("api/player/status")]
        public async Task<PlayerStatus> GetPlayerStatus()
        {
            var grain = _client.GetGrain<IPlayerGrain>(PlayerId);
            var status = await grain.GetStatusAsync();
            return _mappings.Map<PlayerStatus>(status);
        }
    }
}
