﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Grains.Interfaces.Game;
using Grains.Interfaces.Game.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Orleans;
using Web.Infrastructure.Validation;
using Web.Services.Gameplay.Interfaces;
using Web.ViewModels;


namespace Web.Controllers
{
    [Authorize]
    [ApiController]
    public class GameController : BaseApiController
    {
        private readonly IClusterClient _client;
        private readonly IGameListStorage _gameListStorage;
        private readonly IMapper _mappings;

        public GameController(IMapper mappings, IClusterClient client, IGameListStorage gameListStorage)
        {
            _client = client;
            _gameListStorage = gameListStorage;
            _mappings = mappings;
        }

        [HttpPost]
        [Route("api/game")]
        public async Task<GameStatus> CreateGame([FromQuery]string name)
        {
            var id = Guid.NewGuid();
            var grain = _client.GetGrain<IGameGrain>(id);
            await grain.CreateAsync(name);
            var status = await grain.JoinPlayer(PlayerId);
            return _mappings.Map<GameStatus>(status);
        }

        [HttpPost]
        [Route("api/game/{gameId}/join")]
        public async Task<GameStatus> JoinGame(Guid gameId)
        {
            var grain = _client.GetGrain<IGameGrain>(gameId);
            var status = await grain.JoinPlayer(PlayerId);
            return _mappings.Map<GameStatus>(status);
        }

        [HttpPost]
        [Route("api/game/{gameId}/ship")]
        [ValidatePlayerEligibility]
        public async Task<GameStatus> PutShip(Guid gameId, [FromBody]Ship ship)
        {
            var grain = _client.GetGrain<IGameGrain>(gameId);
            var shipModel = _mappings.Map<GrainShip>(ship);
            var status = await grain.PutShip(PlayerId, shipModel);

            return _mappings.Map<GameStatus>(status);
        }

        [HttpPost]
        [Route("api/game/{gameId}/shot")]
        [ValidatePlayerEligibility]
        public async Task<GameStatus> Shot(Guid gameId, [FromBody]Shot shot)
        {
            var grain = _client.GetGrain<IGameGrain>(gameId);

            var status = await grain.Shot(PlayerId, shot.Horizontal, shot.Vertical);

            return _mappings.Map<GameStatus>(status);
        }

        [HttpGet]
        [Route("api/game/{gameId}/status")]
        [ValidatePlayerEligibility]
        public async Task<GameStatus> GetGameStatus(Guid gameId)
        {
            var grain = _client.GetGrain<IGameGrain>(gameId);
            var status = await grain.GetStatusAsync();
            return _mappings.Map<GameStatus>(status);
        }

        [HttpGet]
        [Route("api/games")]
        public Task<IEnumerable<GameInfo>> GetGames()
        {
            var games = _gameListStorage.GetGamesList();
            return Task.FromResult(_mappings.Map<IEnumerable<GameInfo>>(games));
        }
    }
}
