using Microsoft.Extensions.DependencyInjection;
using Web.Services.Gameplay.Implementations;
using Web.Services.Gameplay.Interfaces;

namespace Web.Services.Gameplay
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGameplayDependencies(this IServiceCollection services)
        {
            services.AddSingleton<IGameListStorage, GameListStorage>();
            services.AddSingleton<IGameListStream, GameListStream>();
            services.AddSingleton<IGameStatusStreamProvider, GameStatusStreamProvider>();
            return services;
        }
    }
}