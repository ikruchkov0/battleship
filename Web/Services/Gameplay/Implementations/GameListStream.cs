using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using Grains.Interfaces.GameLobby;
using Web.Services.Gameplay.Interfaces;

namespace Web.Services.Gameplay.Implementations
{
    internal class GameListStream : IGameListStream
    {
        private readonly Subject<IEnumerable<IGameInfo>> _subject;

        public GameListStream()
        {
            _subject = new Subject<IEnumerable<IGameInfo>>();
        }

        public void PushUpdates(IEnumerable<IGameInfo> list)
        {
            _subject.OnNext(list);
        }

        public IObservable<IEnumerable<IGameInfo>> AsObservable()
        {
            return _subject;
        }
    }
}