using System.Collections.Generic;
using System.Linq;
using Grains.Interfaces.GameLobby;
using Microsoft.Extensions.Caching.Memory;
using Web.Services.Gameplay.Interfaces;

namespace Web.Services.Gameplay.Implementations
{
    internal class GameListStorage : IGameListStorage
    {
        private readonly IMemoryCache _cache;

        public GameListStorage(IMemoryCache cache)
        {
            _cache = cache;
        }

        public IEnumerable<IGameInfo> GetGamesList()
        {
            if (_cache.TryGetValue(CacheKeys.GameList, out var cachedGameList))
            {
                return cachedGameList as IEnumerable<IGameInfo>;
            }
            
            return Enumerable.Empty<IGameInfo>();
        }

        public void SetGamesList(IEnumerable<IGameInfo> list)
        {
            _cache.Set(CacheKeys.GameList, list);
        }
    }
}