using System;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using Battleship.Common;
using Grains.Interfaces.Game.Messages;
using Grains.Interfaces.Game.Models;
using Orleans;
using Orleans.Streams;
using Web.Services.Gameplay.Interfaces;

namespace Web.Services.Gameplay.Implementations
{
    internal class GameStatusStreamProvider : IGameStatusStreamProvider
    {
        private readonly IClusterClient _client;

        public GameStatusStreamProvider(IClusterClient client)
        {
            _client = client;
        }
        
        #region Implementation of IGameStatusStreamProvider

        public async Task<IObservable<IGameStatus>> GetStreamAsObservableAsync(Guid gameId, CancellationToken cancellationToken)
        {
            var stream = GetStream(gameId);
            var subject = new Subject<IGameStatus>();

            var subscriptionHandler = await stream.SubscribeAsync((data, token) =>
            {
                subject.OnNext(data.NewStatus);
                return Task.CompletedTask;
            });

            cancellationToken.Register(() =>
            {
                subscriptionHandler.UnsubscribeAsync().GetAwaiter().GetResult();
                subject.OnCompleted();
            });

            return subject;
        }

        #endregion
        
        private IAsyncStream<IGameStatusUpdated> GetStream(Guid gameId)
        {
            var streamProvider = _client.GetStreamProvider(StorageNames.StreamProviderName);
            return streamProvider.GetStream<IGameStatusUpdated>(gameId, StorageNames.GameStream);
        }
    }
}