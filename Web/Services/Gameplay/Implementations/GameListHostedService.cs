using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Battleship.Common;
using Grains.Interfaces.GameLobby;
using Grains.Interfaces.GameLobby.Messages;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using Orleans.Streams;
using Web.Services.Gameplay.Interfaces;

namespace Web.Services.Gameplay.Implementations
{
    internal class GameListHostedService : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly IClusterClient _client;
        private readonly IGameListStorage _storage;
        private readonly IGameListStream _stream;
        private StreamSubscriptionHandle<IGameListUpdated> _handler;

        public GameListHostedService(
            ILogger<GameListHostedService> logger,
            IClusterClient client,
            IGameListStream stream,
            IGameListStorage storage
        )
        {
            _logger = logger;
            _client = client;
            _stream = stream;
            _storage = storage;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Game List Service is starting.");

            await SubscribeAsync();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Game List Service is stopping.");

            await UnsubscribeAsync();
        }

        public void Dispose()
        {
            UnsubscribeAsync().GetAwaiter().GetResult();
        }

        private async Task SubscribeAsync()
        {
            try
            {
                var list = await GetGameListAsync();
                _storage.SetGamesList(list);
                _handler = await GetGameListStream().SubscribeAsync(DataReceivedHandler);
            }
            catch (OrleansException error)
            {
                _logger.LogWarning(error, "Error subscribe");
            }
        }

        private async Task UnsubscribeAsync()
        {
            try
            {
                if (_handler != null)
                {
                    await _handler?.UnsubscribeAsync();
                }
            }
            catch (Exception error)
            {
                _logger.LogWarning(error,
                    "Error gracefully unsubscribe. Will ignore and continue to shutdown.");
            }
        }

        private Task DataReceivedHandler(IGameListUpdated data, StreamSequenceToken token)
        {
            _storage.SetGamesList(data.GameList);
            _stream.PushUpdates(data.GameList);
            return Task.CompletedTask;
        }

        private IAsyncStream<IGameListUpdated> GetGameListStream()
        {
            var lobbyId = GrainNames.MainLobby;
            var streamProvider = _client.GetStreamProvider(StorageNames.StreamProviderName);
            return streamProvider.GetStream<IGameListUpdated>(lobbyId, StorageNames.GameListStream);
        }

        private Task<IEnumerable<IGameInfo>> GetGameListAsync()
        {
            var lobbyId = GrainNames.MainLobby;
            var lobby = _client.GetGrain<IGameLobbyGrain>(lobbyId);
            return lobby.GetGamesAsync();
        }
    }
}