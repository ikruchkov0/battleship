using System.Collections.Generic;
using Grains.Interfaces.GameLobby;

namespace Web.Services.Gameplay.Interfaces
{
    public interface IGameListStorage
    {
        IEnumerable<IGameInfo> GetGamesList();

        void SetGamesList(IEnumerable<IGameInfo> list);
    }
}