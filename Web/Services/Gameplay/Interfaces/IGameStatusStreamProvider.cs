using System;
using System.Threading;
using System.Threading.Tasks;
using Grains.Interfaces.Game.Models;

namespace Web.Services.Gameplay.Interfaces
{
    public interface IGameStatusStreamProvider
    {
        Task<IObservable<IGameStatus>> GetStreamAsObservableAsync(Guid gameId, CancellationToken cancellationToken);
    }
}