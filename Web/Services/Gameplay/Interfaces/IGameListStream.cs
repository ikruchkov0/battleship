using System;
using System.Collections.Generic;
using Grains.Interfaces.GameLobby;

namespace Web.Services.Gameplay.Interfaces
{
    public interface IGameListStream
    {
        IObservable<IEnumerable<IGameInfo>> AsObservable();

        void PushUpdates(IEnumerable<IGameInfo> list);
    }
}