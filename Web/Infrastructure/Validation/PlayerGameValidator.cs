using System;
using System.Linq;
using System.Threading.Tasks;
using Grains.Interfaces.Player;
using Orleans;

namespace Web.Infrastructure.Validation
{
    public class PlayerGameValidator
    {
        private readonly IClusterClient _client;

        public PlayerGameValidator(IClusterClient client)
        {
            _client = client;
        }
        
        public async Task<bool> Validate(Guid gameId, Guid playerId)
        {
            var player = _client.GetGrain<IPlayerGrain>(playerId);
            var status = await player.GetStatusAsync();

            return status.ActiveGames.Any(x => x == gameId);
        }
    }
}