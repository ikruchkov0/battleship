using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Web.Infrastructure.Auth;

namespace Web.Infrastructure.Validation
{
    public class ValidatePlayerEligibilityAttribute : Attribute, IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!await ValidateGameId(context))
            {
                context.Result = new ForbidResult();
                return;
            }

            await next();
        }

        private Guid GetPlayerId(ActionExecutingContext context)
        {
            return context.HttpContext.User.Claims.GetPlayerId();
        }

        private async Task<bool> ValidateGameId(ActionExecutingContext context)
        {
            if (!context.ActionArguments.TryGetValue("gameId", out var gameIdObject))
            {
                return true;
            }

            if (!(gameIdObject is Guid))
            {
                return true;
            }

            var gameId = (Guid)gameIdObject;

            var validator = context.HttpContext.RequestServices.GetService<PlayerGameValidator>();
            var playerId = GetPlayerId(context);

            return await validator.Validate(gameId, playerId);
        }
    }
}