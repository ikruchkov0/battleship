using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Grains.Interfaces.Player.Models;
using Microsoft.IdentityModel.Tokens;
using Web.Settings;

namespace Web.Infrastructure.Auth
{
    public class TokenProvider
    {
        private readonly AppSettings _config;

        public TokenProvider(AppSettings config)
        {
            _config = config;
        }

        public string GenerateToken(IPlayerStatus player)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_config.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new [] 
                {
                    new Claim(ClaimTypes.Name, player.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);

            return token;
        }

    }
}