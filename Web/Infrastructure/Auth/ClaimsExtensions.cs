using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Web.Infrastructure.Auth
{
    public static class ClaimsExtensions
    {
        public static Guid GetPlayerId(this IEnumerable<Claim> claims)
        {
            var playerIdClaim = claims.Where(x => x.Type == ClaimTypes.Name).SingleOrDefault();
            if (playerIdClaim == null)
            {
                throw new UnauthorizedAccessException("Claim not found");
            }

            if (!Guid.TryParse(playerIdClaim.Value, out var playerId))
            {
                throw new UnauthorizedAccessException("Invalid claim");
            }

            return playerId;
        }
    }
}