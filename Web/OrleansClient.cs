using System;
using System.Threading.Tasks;
using Battleship.Common;
using Grains.Game;
using Grains.Interfaces.Game;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

namespace Web
{
    public static class OrleansClient
    {
        public static IClusterClient CreateClusterClient(IServiceProvider serviceProvider)
        {
            var log = serviceProvider.GetService<ILogger<Startup>>();

            var client = new ClientBuilder()
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(GameGrain).Assembly))
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(IGameGrain).Assembly))
                .AddSimpleMessageStreamProvider(StorageNames.StreamProviderName)
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "orleans-battleship";
                    options.ServiceId = "Battleship";
                })
                .UseLocalhostClustering(serviceId: "Battleship", clusterId: "orleans-battleship")
                .Build();

            var retryCount = 0;
            client.Connect(RetryFilter).GetAwaiter().GetResult();
            return client;


            async Task<bool> RetryFilter(Exception exception)
            {
                retryCount++;
                log?.LogWarning("Exception while attempting to connect to Orleans cluster: {Exception}", exception);
                await Task.Delay(TimeSpan.FromSeconds(2));
                return retryCount < 10;
            }
        }
    }
}