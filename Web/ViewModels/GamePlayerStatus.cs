using System;

namespace Web.ViewModels
{
    public class GamePlayerStatus
    {
        public Guid Id { get; set; }

        public FieldStatus FieldStatus { get; set; }
    }
}