namespace Web.ViewModels
{
    public class FieldStatus
    {
        public int TotalShips { get; set; }

        public int ShipsLimit { get; set; }

        public int ShipsLeft { get; set; }
    }
}