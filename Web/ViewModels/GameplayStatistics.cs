namespace Web.ViewModels
{
    public class GameplayStatistics
    {
        public int GamesWon { get; set; }

        public int GamesLost { get; set; }

        public int ShotsFired { get; set; }

        public int ShotsMissed { get; set; }

        public int ShipsKilled { get; set; }
    }
}