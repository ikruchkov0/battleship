using System;
using System.Collections.Generic;

namespace Web.ViewModels
{
    public class PlayerStatus
    {
        public Guid Id { get; set; }


        public string Name { get; set; }


        public bool IsActive { get; set; }


        public IEnumerable<Guid> ActiveGames { get; set; }


        public GameplayStatistics Statistics { get; set; }

    }
}