namespace Web.ViewModels
{
    public class Shot
    {
        public uint Horizontal { get; set; }
        public uint Vertical { get; set; }
    }
}