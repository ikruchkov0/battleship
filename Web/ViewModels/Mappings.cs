﻿using System;
using AutoMapper;
using Battleship.Common.Enums;
using Grains.Interfaces.Game.Models;
using Grains.Interfaces.GameLobby;
using Grains.Interfaces.Player.Models;
using Orleans;
using IGamePlayerStatus = Grains.Interfaces.Game.Models.IPlayerStatus;
using IPlayerStatus = Grains.Interfaces.Player.Models.IPlayerStatus;

namespace Web.ViewModels
{
    public static class Mappings
    {
        public static IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<IGameStatus, GameStatus>();
                cfg.CreateMap<IGamePlayerStatus, GamePlayerStatus>();
                cfg.CreateMap<IFieldStatus, FieldStatus>();
                cfg.CreateMap<IShotHistoryEntry, ShotHistoryEntry>();

                cfg.CreateMap<IPlayerStatus, PlayerStatus>();
                cfg.CreateMap<IPlayerStatus, CreatedPlayer>();
                cfg.CreateMap<IGameplayStatistics, GameplayStatistics>();

                cfg.CreateMap<IGameInfo, GameInfo>();

                cfg.CreateMap<Ship, GrainShip>();
            });

            return config.CreateMapper();
        }
    }
}