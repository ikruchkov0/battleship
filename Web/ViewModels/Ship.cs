namespace Web.ViewModels
{
    public class Ship
    {
        public string Orientation { get; set; }

        public string Type { get; set; }

        public uint Horizontal { get; set; }

        public uint Vertical { get; set; }
    }
}