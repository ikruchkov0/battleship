namespace Web.ViewModels
{
    public class CreatedPlayer : PlayerStatus
    {
        public string Token { get; set; }
    }
}