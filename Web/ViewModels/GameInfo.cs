using System;

namespace Web.ViewModels
{
    public class GameInfo
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}