using Battleship.Common.Enums;
using Grains.Interfaces.Game.Models;

namespace Web.ViewModels
{
    public class GrainShip : IShip
    {
        public ShipOrientation Orientation { get; set; }

        public ShipType Type { get; set; }

        public uint Horizontal { get; set; }

        public uint Vertical { get; set; }
    }
}