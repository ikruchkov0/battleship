using System;

namespace Web.ViewModels
{
    public class ShotHistoryEntry
    {
        public Guid PlayerId { get; set; }

        public Guid PlayerToHitId { get; set; }

        public uint Horizontal { get; set; }

        public uint Vertical { get; set; }

        public bool IsHit { get; set; }

        public bool IsMiss { get; set; }

        public bool IsKill { get; set; }
    }
}