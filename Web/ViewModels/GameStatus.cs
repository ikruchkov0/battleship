using System;
using System.Collections.Generic;

namespace Web.ViewModels
{
    public class GameStatus
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Stage { get; set; }

        public IEnumerable<GamePlayerStatus> Players { get; set; }

        public GamePlayerStatus PlayerTurn { get; set; }

        public GamePlayerStatus PlayerToHit { get; set; }

        public GamePlayerStatus Winner { get; set; }

        public IEnumerable<ShotHistoryEntry> ShotHistory { get; set; }
    }
}