using System.Linq;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Channels;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Web.ViewModels;
using System.Threading;
using Web.Services.Gameplay.Interfaces;

namespace Web.Hubs
{
    public class GameLobbyHub : Hub
    {
        private const int BufferSize = 10;
        private readonly IGameListStream _stream;
        private readonly IMapper _mapper;

        public GameLobbyHub(IGameListStream stream, IMapper mapper)
        {
            _stream = stream;
            _mapper = mapper;
        }

        public ChannelReader<IEnumerable<GameInfo>> StreamGameListUpdates(CancellationToken cancellationToken)
        {
            var stream = _stream.AsObservable().Select(x => x.Select(_mapper.Map<GameInfo>));
            var channel = Channel.CreateBounded<IEnumerable<GameInfo>>(new BoundedChannelOptions(BufferSize)
            {
                SingleWriter = true,
                SingleReader = false,
                FullMode = BoundedChannelFullMode.DropOldest
            });

            var disposable = stream.Subscribe(
                                value => channel.Writer.TryWrite(value),
                                error => channel.Writer.TryComplete(error),
                                () => channel.Writer.TryComplete());

            cancellationToken.Register(() => disposable.Dispose());

            // Complete the subscription on the reader completing
            channel.Reader.Completion.ContinueWith(task => disposable.Dispose());

            return channel.Reader;
        }
    }
}