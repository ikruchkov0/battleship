using System;
using System.Reactive.Linq;
using System.Threading.Channels;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Web.ViewModels;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Web.Infrastructure.Auth;
using Web.Infrastructure.Validation;
using Web.Services.Gameplay.Interfaces;

namespace Web.Hubs
{
    [Authorize]
    public class GameHub : Hub
    {
        private const int BufferSize = 10;
        private readonly IGameStatusStreamProvider _streamProvider;
        private readonly PlayerGameValidator _validator;
        private readonly IMapper _mapper;

        public GameHub(IGameStatusStreamProvider streamProvider, PlayerGameValidator validator, IMapper mapper)
        {
            _streamProvider = streamProvider;
            _validator = validator;
            _mapper = mapper;
        }

        public async Task<ChannelReader<GameStatus>> StreamGameUpdates(Guid gameId, CancellationToken cancellationToken)
        {
            var playerId = Context.User.Claims.GetPlayerId();
            if (!await _validator.Validate(gameId, playerId))
            {
                throw new UnauthorizedAccessException("Player doesn't have permissions to subscribe the game");
            }
            
            var stream = await _streamProvider.GetStreamAsObservableAsync(gameId, cancellationToken);
            
            var observable = stream.Select(_mapper.Map<GameStatus>);
            
            var channel = Channel.CreateBounded<GameStatus>(new BoundedChannelOptions(BufferSize)
            {
                SingleWriter = true,
                SingleReader = false,
                FullMode = BoundedChannelFullMode.DropOldest
            });

            var disposable = observable.Subscribe(
                                value => channel.Writer.TryWrite(value),
                                error => channel.Writer.TryComplete(error),
                                () => channel.Writer.TryComplete());

            cancellationToken.Register(() => disposable.Dispose());

            // Complete the subscription on the reader completing
            _ = channel.Reader.Completion.ContinueWith(task => disposable.Dispose(), cancellationToken);

            return channel.Reader;
        }
    }
}