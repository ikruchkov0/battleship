﻿using System.Net;
using System.Threading.Tasks;
using Grains.Game;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Battleship.Common;

namespace Silo
{
    class Program
    {
        public static Task Main(string[] args)
        {
            return new HostBuilder()
                .UseOrleans(builder =>
                {
                    builder
                        .UseLocalhostClustering(serviceId: "Battleship", clusterId: "orleans-battleship")
                        .Configure<ClusterOptions>(options =>
                        {
                            options.ClusterId = "orleans-battleship";
                            options.ServiceId = "Battleship";
                        })
                        .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)
                        .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(GameGrain).Assembly).WithReferences())
                        .AddSimpleMessageStreamProvider(StorageNames.StreamProviderName)
                        .AddMemoryGrainStorage("PubSubStore")
                        .AddMemoryGrainStorage(StorageNames.TransactionStoreName)
                        .UseTransactions();
                })
                .ConfigureServices((context, services) =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });
                })
                .ConfigureLogging(builder =>
                {
                    builder
                        .AddConsole()
                        .AddFilter(level => level >= LogLevel.Information);
                })
                .RunConsoleAsync();
        }
    }
}
