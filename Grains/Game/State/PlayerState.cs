using System;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using Battleship.Models.Fields;
using Grains.Game.Enums;
using Grains.Game.Models;
using Grains.Interfaces.Game.Models;

namespace Grains.Game.State
{

    internal class PlayerState
    {
        private readonly PreparationField _preparationField;
        private BattleField _battleField;

        public Guid Id { get; }

        public PlayerStage Stage { get; private set; }

        public PlayerState(Guid playerId)
        {
            Id = playerId;
            _preparationField = new PreparationField();
            Stage = PlayerStage.Preparation;
        }

        public void PutShip(IShip ship)
        {
            if (Stage != PlayerStage.Preparation)
            {
                throw new GameException("Wrong player state");
            }

            var statistics = _preparationField.GetStatistics();
            if (statistics.ShipsLeft == 0) 
            {
                throw new GameException("Field already filled");
            }

            _preparationField.Put(NewShipFactory.CreateShip(ship));

            statistics = _preparationField.GetStatistics();
            if (statistics.ShipsLeft == 0)
            {
                _battleField = new BattleField(_preparationField);
                Stage = PlayerStage.InBattle;
            }
        }

        public ShotResult Hit(uint horizontal, uint vertical)
        {
            if (Stage != PlayerStage.InBattle)
            {
                throw new GameException("Wrong player state");
            }

            var result =  _battleField.Shot(horizontal, vertical);
            if (_battleField.LiveShipsCount == 0)
            {
                Stage = PlayerStage.Looser;
            }
            return result;
        }

        public void MarkAsWinner()
        {
            if (Stage != PlayerStage.InBattle)
            {
                throw new GameException($"Unable to mark as winner player with stage {Stage}");
            }
            Stage = PlayerStage.Winner;
        }

        public FieldStatistics CurrentFieldStatistics => GetCurrentField().GetStatistics();

        public IPlayerStatus GetStatus()
        {
            return new PlayerStatus(this);
        }

        private BaseField GetCurrentField()
        {
            switch (Stage)
            {
                case PlayerStage.Preparation:
                    return _preparationField;
                case PlayerStage.Winner:
                case PlayerStage.Looser:
                case PlayerStage.InBattle:
                    return _battleField;
                default:
                    throw new GameException($"Unknown player stage {Stage}");
            }
        }        
    }
}
