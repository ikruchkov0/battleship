using System.Linq;
using System;
using System.Collections.Generic;
using Grains.Interfaces.Game.Models;
using Grains.Game.Models;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using Grains.Game.Enums;

namespace Grains.Game.State
{
    public class GameState
    {
        internal const int PlayersLimit = 2;

        internal Guid Id { get; private set; }
        internal GameStage Stage { get; private set; }
        internal string Name { get; private set; }
        internal IDictionary<Guid, PlayerState> Players { get; private set; }
        internal Guid? Winner { get; private set; }
        internal TurnsRing<Guid> PlayersTurns { get; private set; }
        internal IList<ShotHistoryEntry> ShotHistory { get; private set; }

        public IGameStatus GetStatus()
        {
            return new GameStatus(this);
        }

        public void Initialize(Guid id, string name)
        {
            if (Stage != GameStage.None)
            {
                throw new GameException("Wrong game status");
            }

            Id = id;
            Stage = GameStage.Created;
            Name = name;
            Players = new Dictionary<Guid, PlayerState>(PlayersLimit);
        }

        public void JoinPlayer(Guid playerId)
        {
            if (Stage != GameStage.Created)
            {
                throw new GameException("Wrong game status");
            }

            if (Players.TryGetValue(playerId, out _))
            {
                throw new GameException("Player already added");
            }

            if (Players.Count >= PlayersLimit)
            {
                throw new GameException("Player Limit Exceeded");
            }

            Players[playerId] = new PlayerState(playerId);
        }

        public void PutShip(Guid playerId, IShip ship)
        {
            if (Stage != GameStage.Created)
            {
                throw new GameException("Wrong game status");
            }

            if (!Players.TryGetValue(playerId, out var player))
            {
                throw new GameException("Player not found");
            }

            player.PutShip(ship);

            if (Players.Count != PlayersLimit)
            {
                return;
            }
            
            if (Players.Values.Any(x => x.Stage == PlayerStage.Preparation))
            {
                return;
            }
            
            Stage = GameStage.Started;
            ShotHistory = new List<ShotHistoryEntry>();
            PlayersTurns = new TurnsRing<Guid>(Players.Values.Select(x => x.Id));
        }

        public ShotResult Shot(Guid playerId, uint horizontal, uint vertical)
        {
            if (Stage != GameStage.Started)
            {
                throw new GameException("Wrong game status");
            }

            if (PlayersTurns.Current != playerId)
            {
                throw new GameException("Not this player turn");
            }

            if (!Players.TryGetValue(playerId, out _))
            {
                throw new GameException("Player not found");
            }

            if (!Players.TryGetValue(PlayersTurns.Next, out var playerToHit))
            {
                throw new GameException("Player to hit not found");
            }

            var result = playerToHit.Hit(horizontal, vertical);

            ShotHistory.Add(new ShotHistoryEntry(playerId, playerToHit.Id, result, horizontal, vertical));

            var winners = Players.Values.Where(x => x.Stage != PlayerStage.Looser).ToArray();
            if (winners.Length == 1)
            {   
                var winner = winners[0];
                winner.MarkAsWinner();
                Winner = winner.Id;
                Stage = GameStage.Finished;
            }
            PlayersTurns.NextTurn();
            return result;
        }
    }
}
