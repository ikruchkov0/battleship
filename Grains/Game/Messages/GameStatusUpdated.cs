using Grains.Interfaces.Game.Messages;
using Grains.Interfaces.Game.Models;

namespace Grains.Game.Messages
{
    internal class GameStatusUpdated : IGameStatusUpdated
    {
        public IGameStatus NewStatus { get; set; }
    }
}