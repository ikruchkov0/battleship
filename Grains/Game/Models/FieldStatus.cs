using Battleship.Models.Fields;
using Grains.Interfaces.Game.Models;

namespace Grains.Game.Models
{
    internal class FieldStatus : IFieldStatus
    {
        public FieldStatus(FieldStatistics statistics)
        {
            TotalShips = statistics.TotalShips;
            ShipsLimit = statistics.ShipsLimit;
            ShipsLeft = statistics.ShipsLeft;
        }

        public int TotalShips { get; }
        public int ShipsLimit { get; }
        public int ShipsLeft { get; }
    }
}