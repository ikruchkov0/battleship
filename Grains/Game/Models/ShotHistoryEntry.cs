using System;
using Battleship.Common.Enums;
using Grains.Interfaces.Game.Models;

namespace Grains.Game.Models
{
    internal class ShotHistoryEntry : IShotHistoryEntry
    {
        public ShotHistoryEntry(Guid playerId, Guid playerToHitId, ShotResult shotResult, uint horizontal, uint vertical)
        {
            PlayerId = playerId;
            PlayerToHitId = playerToHitId;
            IsHit = shotResult == ShotResult.Hit;
            IsMiss = shotResult == ShotResult.Miss;
            IsKill = shotResult == ShotResult.Kill;
            Horizontal = horizontal;
            Vertical = vertical;
        }
        
        public Guid PlayerId { get; }
        public Guid PlayerToHitId { get; }
        public uint Horizontal { get; }
        public uint Vertical { get; }
        public bool IsHit { get; }
        public bool IsMiss { get; }
        public bool IsKill { get; }
    }
}