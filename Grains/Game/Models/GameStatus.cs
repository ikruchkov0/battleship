using System.Linq;
using System.Collections.Generic;
using System;
using Battleship.Common.Enums;
using Grains.Interfaces.Game.Models;
using Grains.Game.State;

namespace Grains.Game.Models
{
    internal class GameStatus : IGameStatus
    {
        public GameStatus(GameState state) 
        {
            Id = state.Id;
            Name = state.Name;
            Stage = state.Stage;
            Players = state.Players.Values.Select(x => x.GetStatus()).ToList();
            PlayerTurn = state.PlayersTurns != null ? state.Players[state.PlayersTurns.Current].GetStatus() : null;
            PlayerToHit = state.PlayersTurns != null ? state.Players[state.PlayersTurns.Next].GetStatus() : null;
            Winner = state.Winner.HasValue ? state.Players[state.Winner.Value].GetStatus() : null;
            ShotHistory = state.ShotHistory;
        }

        public Guid Id { get; }
        public string Name { get; }
        public GameStage Stage { get; }
        public IReadOnlyList<IPlayerStatus> Players { get; }
        public int PlayerLimit => GameState.PlayersLimit;
        public IPlayerStatus PlayerTurn { get; }
        public IPlayerStatus PlayerToHit { get; }
        public IPlayerStatus Winner { get; }
        public IEnumerable<IShotHistoryEntry> ShotHistory { get; }
    }
}
