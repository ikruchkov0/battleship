using System;
using Battleship.Models.Ships;
using Grains.Interfaces.Game.Models;
using Battleship.Common.Enums;
using Battleship.Common.Interfaces;

namespace Grains.Game.Models
{
    internal static class NewShipFactory 
    {
        public static IShipData CreateShip(IShip ship)
        {
            switch (ship.Type)
            {
                case ShipType.Battleship:
                    return BattleShip.Create(ship.Orientation, ship.Horizontal, ship.Vertical);
                case ShipType.Cruiser:
                    return Cruiser.Create(ship.Orientation, ship.Horizontal, ship.Vertical);
                case ShipType.Destroyer:
                    return Destroyer.Create(ship.Orientation, ship.Horizontal, ship.Vertical);
                case ShipType.Submarine:
                    return Submarine.Create(ship.Horizontal, ship.Vertical);
                default:
                    throw new ArgumentException($"Unknown ship type {ship.Type}", nameof(ship));
            }
        }
    }
}