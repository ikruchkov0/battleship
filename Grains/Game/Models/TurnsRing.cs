using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Grains.Game.Models
{
    internal class TurnsRing<T> where T: struct 
    {
        private readonly ImmutableArray<T> _items;
        private int _currentIndex;
        public TurnsRing(IEnumerable<T> items)
        {
            var rand = new Random();
            var turns = items.ToArray();
            for(var i = 0; i < turns.Length; i++)
            {
                var index = rand.Next(0, turns.Length);
                var p = turns[i];
                turns[i] = turns[index];
                turns[index] = p;
            }
            _items = turns.ToImmutableArray();
            _currentIndex = 0;
        }

        public T Current => _items[_currentIndex];

        public T Next => _items[GetNextIndex()];

        public void NextTurn()
        {
            _currentIndex = GetNextIndex();
        }

        private int GetNextIndex()
        {
            var result = _currentIndex + 1;
            if (result >= _items.Length)
            {
                result = 0;
            }
            return result;
        }
    }
}