using System;
using Grains.Interfaces.Game.Models;
using Grains.Game.State;

namespace Grains.Game.Models
{
    internal class PlayerStatus : IPlayerStatus
    {
        public PlayerStatus(PlayerState state)
        {
            Id = state.Id;
            FieldStatus = new FieldStatus(state.CurrentFieldStatistics);
        }

        public Guid Id { get; }

        public IFieldStatus FieldStatus { get; }
    }
}
