namespace Grains.Game.Enums
{
    internal enum PlayerStage
    {
        Preparation, InBattle, Winner, Looser
    }
}
