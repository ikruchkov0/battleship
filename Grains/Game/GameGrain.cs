﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;
using Battleship.Common.Enums;
using Grains.Interfaces.Game.Models;
using Battleship.Common.Exceptions;
using Grains.Game.State;
using Grains.Interfaces.Player;
using Grains.Game.Enums;
using Orleans.Transactions.Abstractions;
using Battleship.Common;
using Grains.Interfaces.GameLobby;
using Grains.Interfaces.Game;
using Orleans.Streams;
using Grains.Interfaces.Game.Messages;
using Grains.Game.Messages;

namespace Grains.Game
{
    public class GameGrain : Grain, IGameGrain
    {
        private readonly ITransactionalState<GameState> _state;
        private IAsyncStream<IGameStatusUpdated> _stream;

        public GameGrain(
            [TransactionalState(StorageNames.GameStateName, StorageNames.TransactionStoreName)]
            ITransactionalState<GameState> state
        )
        {
            _state = state;
        }

        public override Task OnActivateAsync()
        {
            var gameId = this.GetPrimaryKey();
            var streamProvider = GetStreamProvider(StorageNames.StreamProviderName);
            _stream = streamProvider.GetStream<IGameStatusUpdated>(gameId, StorageNames.GameStream);
            return base.OnActivateAsync();
        }

        public Task<IGameStatus> GetStatusAsync()
        {
            return _state.PerformRead(state => state.GetStatus());
        }

        public async Task<IGameStatus> CreateAsync(string name)
        {
            var gameId = this.GetPrimaryKey();
            await _state.PerformUpdate(state => state.Initialize(gameId, name));

            var lobby = GrainFactory.GetGrain<IGameLobbyGrain>(GrainNames.MainLobby);
            await lobby.AddGameAsync(gameId, name);

            var status = await _state.PerformRead(state => state.GetStatus());
            await _stream.OnNextAsync(new GameStatusUpdated { NewStatus = status });
            return status;
        }

        public async Task<IGameStatus> JoinPlayer(Guid playerId)
        {
            var player = GrainFactory.GetGrain<IPlayerGrain>(playerId);
            await _state.PerformUpdate(state => state.JoinPlayer(playerId));
            await player.JoinGameAsync(this.GetPrimaryKey());

            var status = await _state.PerformRead(state => state.GetStatus());
            if (status.Players.Count >= status.PlayerLimit)
            {
                var lobby = GrainFactory.GetGrain<IGameLobbyGrain>(GrainNames.MainLobby);
                await lobby.RemoveGameAsync(status.Id);
            }
            await _stream.OnNextAsync(new GameStatusUpdated { NewStatus = status });
            return status;
        }

        public async Task<IGameStatus> PutShip(Guid playerId, IShip ship)
        {
            await _state.PerformUpdate(state => state.PutShip(playerId, ship));

            var status = await _state.PerformRead(state => state.GetStatus());
            await _stream.OnNextAsync(new GameStatusUpdated { NewStatus = status });
            return status;
        }

        public async Task<IGameStatus> Shot(Guid playerId, uint horizontal, uint vertical)
        {
            var shotResult = await _state.PerformUpdate(state => state.Shot(playerId, horizontal, vertical));

            var player = GrainFactory.GetGrain<IPlayerGrain>(playerId);
            await player.AddShotAsync(this.GetPrimaryKey(), shotResult);

            var currentState = await _state.PerformRead(s => s);
            if (currentState.Stage == GameStage.Finished)
            {
                await EndGameForPlayers(currentState.Players.Values);
            }

            var status = currentState.GetStatus();
            await _stream.OnNextAsync(new GameStatusUpdated { NewStatus = status });
            return status;
        }

        private Task EndGameForPlayers(IEnumerable<PlayerState> players)
        {
            var playerNotifications = players.Select(async playerState =>
            {
                var player = GrainFactory.GetGrain<IPlayerGrain>(playerState.Id);
                switch (playerState.Stage)
                {
                    case PlayerStage.Looser:
                        await player.EndGameAsync(this.GetPrimaryKey(), hasWon: false);
                        break;
                    case PlayerStage.Winner:
                        await player.EndGameAsync(this.GetPrimaryKey(), hasWon: true);
                        break;
                    default:
                        throw new GameException($"Unknown player status {playerState.Stage}");
                }
            });
            return Task.WhenAll(playerNotifications);
        }
        
    }
}
