using System;
using Grains.Interfaces.GameLobby;

namespace Grains.GameLobby.Models
{
    internal class GameInfo : IGameInfo
    {
        internal GameInfo(Guid id, string name)
        {
            Id = id;
            Name = name;

        }
        public Guid Id { get; }

        public string Name { get; }
    }
}