using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Battleship.Common;
using Grains.GameLobby.State;
using Grains.Interfaces.GameLobby;
using Orleans;
using Orleans.Transactions.Abstractions;
using Grains.Interfaces.GameLobby.Messages;
using Orleans.Streams;
using Grains.GameLobby.Messages;

namespace Grains.GameLobby
{
    public class GameLobbyGrain : Grain, IGameLobbyGrain
    {
        private readonly ITransactionalState<GameLobbyState> _state;
        private IAsyncStream<IGameListUpdated> _stream;

        public GameLobbyGrain(
            [TransactionalState(StorageNames.GameLobbyStateName, StorageNames.TransactionStoreName)]
            ITransactionalState<GameLobbyState> state
        )
        {
            _state = state;
        }
        
        public override Task OnActivateAsync()
        {
            var lobbyId = this.GetPrimaryKey();
            var streamProvider = GetStreamProvider(StorageNames.StreamProviderName);
            _stream = streamProvider.GetStream<IGameListUpdated>(lobbyId, StorageNames.GameListStream);
            return base.OnActivateAsync();
        }

        public async Task<IEnumerable<IGameInfo>> AddGameAsync(Guid id, string name)
        {
            var list = await _state.PerformUpdate(state => state.AddGame(id, name));
            await PublishUpdate(list);
            return list;
        }

        public Task<IEnumerable<IGameInfo>> GetGamesAsync()
        {
            return _state.PerformRead(state => state.GetList().AsEnumerable());
        }

        public async Task<IEnumerable<IGameInfo>> RemoveGameAsync(Guid id)
        {
            var list = await _state.PerformUpdate(state => state.RemoveGame(id));
            await PublishUpdate(list);
            return list;
        }

        private async Task PublishUpdate(IEnumerable<IGameInfo> list)
        {
            await _stream.OnNextAsync(new GameListUpdated {GameList = list});
            //return Task.CompletedTask;
        }
    }
}