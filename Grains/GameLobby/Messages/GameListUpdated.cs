using System.Collections.Generic;
using Grains.Interfaces.GameLobby;
using Grains.Interfaces.GameLobby.Messages;

namespace Grains.GameLobby.Messages
{
    public class GameListUpdated : IGameListUpdated
    {
        public IEnumerable<IGameInfo> GameList { get; set; }
    }
}