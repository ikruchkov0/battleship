using System;
using System.Collections.Generic;
using System.Linq;
using Grains.GameLobby.Models;
using Grains.Interfaces.GameLobby;

namespace Grains.GameLobby.State
{
    public class GameLobbyState
    {
        private IDictionary<Guid, GameInfo> Games { get; }

        public GameLobbyState()
        {
            Games = new Dictionary<Guid, GameInfo>();
        }

        public IReadOnlyList<IGameInfo> AddGame(Guid id, string name)
        {
            Games.Add(id, new GameInfo(id, name));
            return GetList();
        }

        public IReadOnlyList<IGameInfo> RemoveGame(Guid id)
        {
            Games.Remove(id);
            return GetList();
        }

        public IReadOnlyList<IGameInfo> GetList()
        {
            return Games.Values.Cast<IGameInfo>().ToList();
        }
    }
}