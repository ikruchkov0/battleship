using System;
using System.Collections.Generic;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using Grains.Interfaces.Player.Models;
using Grains.Player.Models;

namespace Grains.Player.State
{
    public class PlayerState
    {
        internal Guid Id { get; private set; }
        internal string Name { get; private set; }
        internal bool IsActive { get; private set; }
        internal IList<Guid> ActiveGames { get; private set; }
        internal GameplayStatistics Statistics { get; private set; }

        public void Create(Guid playerId, string name)
        {
            if (IsActive)
            {
                throw new PlayerException("Unable to create active player");
            }

            Id = playerId;
            Name = name;
            IsActive = true;
            ActiveGames = new List<Guid>();
            Statistics = new GameplayStatistics();
        }
        
        public void JoinGame(Guid gameId)
        {
            if (!IsActive)
            {
                throw new PlayerException("Player is inactive");
            }

            if (ActiveGames.Contains(gameId))
            {
                throw new PlayerException("Already joined game");
            }

            ActiveGames.Add(gameId);
        }

        public void EndGame(Guid gameId, bool hasWon)
        {
            if (!IsActive)
            {
                throw new PlayerException("Player is inactive");
            }

            if (!ActiveGames.Remove(gameId)) 
            {
                throw new PlayerException("Unable to end game. Game not found");
            }
            Statistics.EndGame(hasWon);
        }
        
        public void AddShot(Guid gameId, ShotResult shotResult)
        {
            if (!IsActive)
            {
                throw new PlayerException("Player is inactive");
            }

            if (!ActiveGames.Contains(gameId))
            {
                throw new PlayerException("Game not found");
            }

            Statistics.AddShot(shotResult);
        }

        public IPlayerStatus GetStatus()
        {
            return new PlayerStatus(this);
        }
    }
}