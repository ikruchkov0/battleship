using System;
using System.Collections.Generic;
using Grains.Interfaces.Player.Models;
using Grains.Player.State;

namespace Grains.Player.Models
{
    internal class PlayerStatus : IPlayerStatus
    {
        public PlayerStatus(PlayerState state)
        {
            Id = state.Id;
            Name = state.Name;
            IsActive = state.IsActive;
            ActiveGames = state.ActiveGames;
            Statistics = state.Statistics;
        }

        public Guid Id { get; }
        public string Name { get; }
        public bool IsActive{ get; }
        public IEnumerable<Guid> ActiveGames { get; }
        public IGameplayStatistics Statistics { get; }
        
    }
}