using Battleship.Common.Enums;
using Grains.Interfaces.Player.Models;

namespace Grains.Player.Models
{
    internal class GameplayStatistics : IGameplayStatistics
    {
        public int GamesWon { get; private set; }

        public int GamesLost { get; private set; }

        public int ShotsFired { get; private set; }

        public int ShotsMissed { get; private set; }

        public int ShipsKilled { get; private set; }

        public void EndGame(bool hasWon)
        {
            if (hasWon)
            {
                GamesWon++;
            } 
            else 
            {
                GamesLost++;
            }
        }

        public void AddShot(ShotResult shotResult)
        {
            ShotsFired++;
            switch (shotResult)
            {
                case ShotResult.Kill:
                    ShipsKilled++;
                    break;
                case ShotResult.Miss:
                    ShotsMissed++;
                    break;
            }
        }
    }
}