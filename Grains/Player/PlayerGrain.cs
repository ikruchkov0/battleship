using System;
using System.Threading.Tasks;
using Battleship.Common;
using Battleship.Common.Enums;
using Grains.Interfaces.Player;
using Grains.Interfaces.Player.Models;
using Grains.Player.State;
using Orleans;
using Orleans.Transactions.Abstractions;

namespace Grains.Player
{
    public class PlayerGrain : Grain, IPlayerGrain
    {

        private readonly ITransactionalState<PlayerState> _state;
        public PlayerGrain(
            [TransactionalState(StorageNames.PlayerStateName, StorageNames.TransactionStoreName)]
            ITransactionalState<PlayerState> state)
        {
            _state = state;
        }

        public async Task<IPlayerStatus> CreateAsync(string name)
        {
            await _state.PerformUpdate(state => state.Create(this.GetPrimaryKey(), name));

            var status = await _state.PerformRead(state => state.GetStatus());
            return status;
        }

        public async Task<IPlayerStatus> JoinGameAsync(Guid gameId)
        {
            await _state.PerformUpdate(state => state.JoinGame(gameId));

            var status = await _state.PerformRead(state => state.GetStatus());
            return status;
        }

        public async Task<IPlayerStatus> EndGameAsync(Guid gameId, bool hasWon)
        {
            await _state.PerformUpdate(state => state.EndGame(gameId, hasWon));

            var status = await _state.PerformRead(state => state.GetStatus());
            return status;
        }

        public async Task<IPlayerStatus> AddShotAsync(Guid gameId, ShotResult shotResult)
        {
            await _state.PerformUpdate(state => state.AddShot(gameId, shotResult));

            var status = await _state.PerformRead(state => state.GetStatus());
            return status;
        }

        public Task<IPlayerStatus> GetStatusAsync()
        {
            return _state.PerformRead(state => state.GetStatus());
        }
    }
}