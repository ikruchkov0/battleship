using System;
using Battleship.Common.Enums;
using Battleship.Exceptions;
using Battleship.Models.Fields;
using Battleship.Models.Ships;
using FluentAssertions;
using Xunit;

namespace Battleship.Tests
{
    public class PreparationFieldTests
    {
        [Fact]
        public void PutSingleShip()
        {
            var field = new PreparationField();

            field.Put(TestData.SubmarineAtStart);
        }

        [Fact]
        public void PutSamePosition()
        {
            var field = new PreparationField();

            field.Put(TestData.SubmarineAtStart);

            field.Invoking(x => x.Put(TestData.SubmarineAtStart)).Should().Throw<PositionOccupiedException>();
        }

        [Fact]
        public void PutSameType()
        {
            var field = new PreparationField();

            field.Put(TestData.VerticalBattleshipAtLeft);

            field.Invoking(x => x.Put(TestData.VerticalBattleshipAtRight)).Should().Throw<ShipLimitException>();
        }

        [Fact]
        public void PutShipWithIntersectionMiddle()
        {
            var ship1 = Cruiser.Create(ShipOrientation.Horizontal, 4, 4);
            var ship2 = Cruiser.Create(ShipOrientation.Vertical, ship1.Horizontal + 1, ship1.Vertical - 1);
            var field = new PreparationField();

            field.Put(ship1);

            field.Invoking(x => x.Put(ship2)).Should().Throw<PositionOccupiedException>();
        }

        [Fact]
        public void PutShipWithIntersectionEnd()
        {
            var ship1 = Cruiser.Create(ShipOrientation.Horizontal, 4, 4);
            var ship2 = Cruiser.Create(ShipOrientation.Vertical, ship1.Horizontal + Cruiser.ShipSize - 1, ship1.Vertical - 1);
            var field = new PreparationField();

            field.Put(ship1);

            field.Invoking(x => x.Put(ship2)).Should().Throw<PositionOccupiedException>();
        }

        [Fact]
        public void PutShipWithIntersectionStart()
        {
            var ship1 = Cruiser.Create(ShipOrientation.Horizontal, 4, 4);
            var ship2 = Cruiser.Create(ShipOrientation.Vertical, ship1.Horizontal, ship1.Vertical - 1);
            var field = new PreparationField();

            field.Put(ship1);

            field.Invoking(x => x.Put(ship2)).Should().Throw<PositionOccupiedException>();
        }

        [Fact]
        public void PutOutOfBounds()
        {
            var ship1 = BattleShip.Create(ShipOrientation.Vertical, 9, 9);
            var field = new PreparationField();

            field.Invoking(x => x.Put(ship1)).Should().Throw<IndexOutOfRangeException>();
        }
    }
}
