using Battleship.Common.Enums;
using Battleship.Common.Interfaces;
using Battleship.Models.Ships;

namespace Battleship.Tests
{
    public static class TestData
    {
        public static IShipData SubmarineAtStart => Submarine.Create(1, 1);
        public static IShipData HorizontalDestroyerAtStart => Destroyer.Create(ShipOrientation.Horizontal, 1, 1);

        public static IShipData HorizontalCruiserAtMiddle => Cruiser.Create(ShipOrientation.Horizontal, 5, 5);
        public static IShipData VerticalCruiserAtMiddle => Cruiser.Create(ShipOrientation.Vertical, 5, 5);
        public static IShipData VerticalBattleshipAtLeft => BattleShip.Create(ShipOrientation.Vertical, 1, 1);
        public static IShipData VerticalBattleshipAtRight => BattleShip.Create(ShipOrientation.Vertical, 9, 1);
        
    }
}