using Battleship.Common.Enums;
using Battleship.Models.Fields;
using FluentAssertions;
using Xunit;

namespace Battleship.Tests
{
    public class BattlefieldTests
    {
        [Fact]
        public void LiveShipsCount()
        {
            var preparation = new PreparationField();
            preparation.Put(TestData.SubmarineAtStart);
            var battlefield = new BattleField(preparation);

            battlefield.LiveShipsCount.Should().Be(1);
        }

        [Fact]
        public void HitShip()
        {
            var testShip = TestData.HorizontalDestroyerAtStart;
            var preparation = new PreparationField();
            preparation.Put(testShip);
            
            var battlefield = new BattleField(preparation);

            battlefield.Shot(testShip.Horizontal, testShip.Vertical).Should().Be(ShotResult.Hit);
        }

        [Fact]
        public void MissShip()
        {
            var testShip = TestData.SubmarineAtStart;
            var preparation = new PreparationField();
            preparation.Put(testShip);
            var battlefield = new BattleField(preparation);

            battlefield.Shot(testShip.Horizontal + 1, testShip.Vertical + 1).Should().Be(ShotResult.Miss);
        }

        [Fact]
        public void KillShip()
        {
            var testShip = TestData.SubmarineAtStart;
            var preparation = new PreparationField();
            preparation.Put(testShip);
            var battlefield = new BattleField(preparation);

            battlefield.Shot(testShip.Horizontal, testShip.Vertical).Should().Be(ShotResult.Kill);

            battlefield.LiveShipsCount.Should().Be(0);
        }
    }
}
