using System;
using System.Collections.Generic;
using Battleship.Common.Enums;
using Battleship.Common.Interfaces;
using Battleship.Models.Ships;
using FluentAssertions;
using Xunit;

namespace Battleship.Tests
{
    public class ShipTests
    {
        [Fact]
        public void LifeCounterDecrease()
        {
            var ship = new Ship(TestData.HorizontalDestroyerAtStart);

            var lifeBefore = ship.Life;

            ship.Hit(ship.Horizontal, ship.Vertical);

            ship.Life.Should().Be(lifeBefore - 1);
        }

        [Fact]
        public void LifeCounterKill()
        {
            var ship = new Ship(TestData.SubmarineAtStart);

            ship.Hit(ship.Horizontal, ship.Vertical);

            ship.Life.Should().Be(0);
        }

        [Fact]
        public void HitStart()
        {
            var ship = new Ship(TestData.HorizontalCruiserAtMiddle);

            ship.Hit(ship.Horizontal, ship.Vertical);

            ship.HasHit(ship.Horizontal, ship.Vertical).Should().BeTrue();
        }

        [Fact]
        public void HitEnd()
        {
            var ship = new Ship(TestData.HorizontalCruiserAtMiddle);

            ship.Hit(ship.Horizontal + (uint)ship.Size - 1, ship.Vertical);

            ship.HasHit(ship.Horizontal + (uint)ship.Size - 1, ship.Vertical).Should().BeTrue();
        }
        
        public static IEnumerable<object[]> GetHitOutOfBoundsParams()
        {
            var horizontalShip = TestData.HorizontalCruiserAtMiddle;
            var verticalShip = TestData.VerticalCruiserAtMiddle;
            
            yield return new object[] { horizontalShip, horizontalShip.Horizontal - 1, horizontalShip.Vertical, "horizontal" };
            yield return new object[] { horizontalShip, horizontalShip.Horizontal + Cruiser.ShipSize, horizontalShip.Vertical, "horizontal" };
            yield return new object[] { horizontalShip, horizontalShip.Horizontal, horizontalShip.Vertical - 1, "vertical" };
            
            yield return new object[] { verticalShip, verticalShip.Horizontal, verticalShip.Vertical - 1, "vertical" };
            yield return new object[] { verticalShip, verticalShip.Horizontal, verticalShip.Vertical + Cruiser.ShipSize, "vertical"};
            yield return new object[] { verticalShip, verticalShip.Horizontal - 1, verticalShip.Vertical, "horizontal" };
        }

        [Theory]
        [MemberData(nameof(GetHitOutOfBoundsParams))]
        public void HitOutOfBounds(IShipData data, uint horizontal, uint vertical, string wrongParamName)
        {
            var ship = new Ship(data);

            ship.Invoking(x => x.Hit(horizontal, vertical)).Should()
                .Throw<ArgumentException>()
                .Which.ParamName.Should()
                .Be(wrongParamName);
        }

        [Fact]
        public void CheckHit()
        {
            var ship = new Ship(TestData.HorizontalCruiserAtMiddle);

            ship.HasHit(ship.Horizontal, ship.Vertical).Should().BeFalse();
        }

        [Fact]
        public void CheckHitOutOfBounds()
        {
            var ship = new Ship(TestData.HorizontalCruiserAtMiddle);

            ship.HasHit(ship.Horizontal - 1, ship.Vertical - 1).Should().BeFalse();
        }
    }
}
