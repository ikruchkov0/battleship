using System;
using System.Threading.Tasks;
using Battleship.Common.Enums;
using Grains.Interfaces.Player.Models;
using Orleans;

namespace Grains.Interfaces.Player
{
    public interface IPlayerGrain : IGrainWithGuidKey
    {
        [Transaction(TransactionOption.Create)]
        Task<IPlayerStatus> CreateAsync(string name);

        [Transaction(TransactionOption.Create)]
        Task<IPlayerStatus> GetStatusAsync();

        [Transaction(TransactionOption.Join)]
        Task<IPlayerStatus> JoinGameAsync(Guid gameId);

        [Transaction(TransactionOption.Join)]
        Task<IPlayerStatus> EndGameAsync(Guid gameId, bool hasWon);

        [Transaction(TransactionOption.Join)]
        Task<IPlayerStatus> AddShotAsync(Guid gameId, ShotResult shotResult);
    }
}