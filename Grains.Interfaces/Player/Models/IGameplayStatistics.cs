namespace Grains.Interfaces.Player.Models
{
    public interface IGameplayStatistics
    {
        int GamesWon { get; }
        int GamesLost { get; }
        int ShotsFired { get; }
        int ShotsMissed { get; }
        int ShipsKilled { get; }
    }
}