using System;
using System.Collections.Generic;

namespace Grains.Interfaces.Player.Models
{

    public interface IPlayerStatus
    {
        Guid Id { get; }
        string Name { get; }
        bool IsActive { get; }
        IEnumerable<Guid> ActiveGames { get; }
        IGameplayStatistics Statistics { get; }
    }
}