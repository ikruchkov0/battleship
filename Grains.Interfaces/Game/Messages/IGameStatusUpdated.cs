using Grains.Interfaces.Game.Models;

namespace Grains.Interfaces.Game.Messages
{
    public interface IGameStatusUpdated
    {
        IGameStatus NewStatus { get; }
    }
}