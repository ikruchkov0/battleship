using System;

namespace Grains.Interfaces.Game.Models
{
    public interface IShotHistoryEntry
    {
        Guid PlayerId { get; }
        Guid PlayerToHitId { get; }
        uint Horizontal { get; }
        uint Vertical { get; }
        bool IsHit { get; }
        bool IsMiss { get; }
        bool IsKill { get; }
    }
}