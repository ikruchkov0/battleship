using System;
using System.Collections.Generic;
using Battleship.Common.Enums;

namespace Grains.Interfaces.Game.Models
{
    public interface IGameStatus
    {
        Guid Id { get; }
        string Name { get; }
        GameStage Stage { get; }
        int PlayerLimit { get; }
        IReadOnlyList<IPlayerStatus> Players { get; }
        IPlayerStatus PlayerTurn { get; }
        IPlayerStatus PlayerToHit { get; }
        IPlayerStatus Winner { get; }
        IEnumerable<IShotHistoryEntry> ShotHistory { get; }
    }
}