using System;

namespace Grains.Interfaces.Game.Models
{
    public interface IPlayerStatus
    {
        Guid Id { get; }
        IFieldStatus FieldStatus { get; }
    }
}