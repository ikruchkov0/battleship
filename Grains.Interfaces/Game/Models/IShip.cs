using Battleship.Common.Enums;

namespace Grains.Interfaces.Game.Models
{
    public interface IShip
    {
        ShipOrientation Orientation { get; }
        ShipType Type { get; }
        uint Horizontal { get; }
        uint Vertical { get; }
    }
}