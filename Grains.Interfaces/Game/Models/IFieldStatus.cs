namespace Grains.Interfaces.Game.Models
{
    public interface IFieldStatus
    {
        int TotalShips { get; }
        int ShipsLimit { get; }
        int ShipsLeft { get; }
    }
}