﻿using System;
using System.Threading.Tasks;
using Grains.Interfaces.Game.Models;
using Orleans;

namespace Grains.Interfaces.Game
{
    public interface IGameGrain : IGrainWithGuidKey
    {
        [Transaction(TransactionOption.Create)]
        Task<IGameStatus> CreateAsync(string name);

        [Transaction(TransactionOption.Create)]
        Task<IGameStatus> GetStatusAsync();

        [Transaction(TransactionOption.Create)]
        Task<IGameStatus> JoinPlayer(Guid playerId);

        [Transaction(TransactionOption.Create)]
        Task<IGameStatus> PutShip(Guid playerId, IShip ship);

        [Transaction(TransactionOption.Create)]
        Task<IGameStatus> Shot(Guid playerId, uint horizontal, uint vertical);
    }
}
