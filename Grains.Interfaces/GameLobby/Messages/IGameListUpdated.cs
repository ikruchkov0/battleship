using System.Collections.Generic;

namespace Grains.Interfaces.GameLobby.Messages
{
    public interface IGameListUpdated
    {
        IEnumerable<IGameInfo> GameList { get; }
    }
}