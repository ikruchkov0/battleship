using System;

namespace Grains.Interfaces.GameLobby
{
    public interface IGameInfo
    {
        Guid Id { get; }
        string Name { get; }
    }
}