using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orleans;

namespace Grains.Interfaces.GameLobby
{
    public interface IGameLobbyGrain : IGrainWithGuidKey
    {
        [Transaction(TransactionOption.Join)]
        Task<IEnumerable<IGameInfo>> AddGameAsync(Guid id, string name);

        [Transaction(TransactionOption.Join)]
        Task<IEnumerable<IGameInfo>> RemoveGameAsync(Guid id);

        [Transaction(TransactionOption.Create)]
        Task<IEnumerable<IGameInfo>> GetGamesAsync();
    }
}