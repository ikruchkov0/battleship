using System;
using System.Runtime.Serialization;
using Battleship.Common.Enums;

namespace Battleship.Exceptions
{
    [Serializable]
    public class ShipLimitException: Exception
    {
        public ShipLimitException(ShipType type, int limit): base($"Ship {type} limit {limit} reached") {}

        public ShipLimitException(SerializationInfo info, StreamingContext context) : base(info,context)
        {
        }
    }
}