using System;
using System.Runtime.Serialization;

namespace Battleship.Exceptions
{
    [Serializable]
    public class PositionOccupiedException: Exception
    {
        public PositionOccupiedException(): base("Position already occupied") {}
        
        public PositionOccupiedException(SerializationInfo info, StreamingContext context) : base(info,context)
        {
        }
    }
}