using Battleship.Common.Enums;
using Battleship.Common.Interfaces;

namespace Battleship.Models.Ships
{
    public class BattleShip : Ship
    {
        public const int ShipSize = 4;
        public const int ShipsLimit = 1;
        private BattleShip(ShipType type, int size, int limit, ShipOrientation orientation, uint horizontal, uint vertical) : base(type, size, limit, orientation, horizontal, vertical)
        {
        }

        public static IShipData Create(ShipOrientation orientation, uint horizontal, uint vertical)
        {
            return new BattleShip(ShipType.Battleship, ShipSize, ShipsLimit, orientation, horizontal, vertical);
        }
    }

    public class Cruiser : Ship
    {
        public const int ShipSize = 3;
        public const int ShipsLimit = 2;
        private Cruiser(ShipType type, int size, int limit, ShipOrientation orientation, uint horizontal, uint vertical) : base(type, size, limit, orientation, horizontal, vertical)
        {
        }

        public static IShipData Create(ShipOrientation orientation, uint horizontal, uint vertical)
        {
            return new Cruiser(ShipType.Cruiser, ShipSize, ShipsLimit, orientation, horizontal, vertical);
        }
    }

    public class Destroyer : Ship
    {
        public const int ShipSize = 2;
        public const int ShipsLimit = 3;
        private Destroyer(ShipType type, int size, int limit, ShipOrientation orientation, uint horizontal, uint vertical) : base(type, size, limit, orientation, horizontal, vertical)
        {
        }

        public static IShipData Create(ShipOrientation orientation, uint horizontal, uint vertical)
        {
            return new Destroyer(ShipType.Destroyer, ShipSize, ShipsLimit, orientation, horizontal, vertical);
        }
    }

    public class Submarine : Ship
    {
        public const int ShipSize = 1;
        public const int ShipsLimit = 4;
        private Submarine(ShipType type, int size, int limit, ShipOrientation orientation, uint horizontal, uint vertical) : base(type, size, limit, orientation, horizontal, vertical)
        {
        }

        public static IShipData Create(uint horizontal, uint vertical)
        {
            return new Submarine(ShipType.Submarine, ShipSize, ShipsLimit, ShipOrientation.Horizontal, horizontal, vertical);
        }
    }
}