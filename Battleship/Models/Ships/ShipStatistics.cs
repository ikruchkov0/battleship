using System.Collections.Generic;
using System.Linq;
using Battleship.Common.Enums;

namespace Battleship.Models.Ships
{
    public class ShipStatistics
    {
        public ShipStatistics(ShipType type, int shipsLimit, IEnumerable<Ship> ships)
        {
            Limit = shipsLimit;
            Total = ships.Count(x => x.Type == type);
        }

        public int Limit { get; }

        public int Total { get; }
    }
}