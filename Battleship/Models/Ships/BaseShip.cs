using Battleship.Common.Enums;
using Battleship.Common.Interfaces;

namespace Battleship.Models.Ships
{
    public abstract class BaseShip : IShipData
    {
        protected BaseShip(ShipType type, int size, int limit, ShipOrientation orientation, uint horizontal, uint vertical)
        {
            Orientation = orientation;
            Horizontal = horizontal;
            Vertical = vertical;
            Type = type;
            Limit = limit;
            Size = size;
        }

        protected BaseShip(IShipData ship)
        {
            Orientation = ship.Orientation;
            Horizontal = ship.Horizontal;
            Vertical = ship.Vertical;
            Type = ship.Type;
            Limit = ship.Limit;
            Size = ship.Size;
        }

        public ShipOrientation Orientation { get; }
        public ShipType Type { get; }
        public int Limit { get; }
        public int Size { get; }
        public uint Horizontal { get; }
        public uint Vertical { get; }
    }
}