using System;
using System.Linq;
using Battleship.Common.Enums;
using System.Collections.Generic;
using Battleship.Common.Interfaces;

namespace Battleship.Models.Ships
{
    public class Ship : BaseShip
    {
        private readonly Dictionary<(uint, uint), bool> _hits = new Dictionary<(uint, uint), bool>();

        public Ship(IShipData ship) : base(ship) 
        {
        }

        protected Ship(ShipType type, int size, int limit, ShipOrientation orientation, uint horizontal, uint vertical) 
            : base(type, size, limit, orientation, horizontal, vertical) { }
        
        public int Life => Size - _hits.Count(x => x.Value);

        public void Hit(uint horizontal, uint vertical)
        {
            switch (Orientation)
            {
                case ShipOrientation.Horizontal:
                    if (horizontal < Horizontal || horizontal >= Horizontal + Size)
                    {
                        throw new ArgumentException("Out of ship", nameof(horizontal));
                    }
                    if (vertical != Vertical)
                    {
                        throw new ArgumentException("Out of ship", nameof(vertical));
                    }
                    break;
                case ShipOrientation.Vertical:
                    if (horizontal != Horizontal)
                    {
                        throw new ArgumentException("Out of ship", nameof(horizontal));
                    }
                    if (vertical < Vertical || vertical >= Vertical + Size)
                    {
                        throw new ArgumentException("Out of ship", nameof(vertical));
                    }
                    break;
            }

            if (_hits.TryGetValue((horizontal, vertical), out var hasHit) && hasHit)
            {
                return;
            }

            _hits[(horizontal, vertical)] = true;
        }

        public bool HasHit(uint horizontal, uint vertical)
        {
            return _hits.TryGetValue((horizontal, vertical), out var hasHit) && hasHit;
        }
    }
}