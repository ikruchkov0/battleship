using System;
using Battleship.Common.Enums;
using Battleship.Common.Interfaces;
using Battleship.Exceptions;
using Battleship.Models.Ships;

namespace Battleship.Models.Fields
{
    public class PreparationField : BaseField
    {
        public void Put(IShipData newShip)
        {
            ValidateCords(newShip.Horizontal, newShip.Vertical);

            var count = GetShipsCount(newShip.Type);
            if (count == newShip.Limit)
            {
                throw new ShipLimitException(newShip.Type, newShip.Limit);
            }

            if (Field[newShip.Horizontal, newShip.Vertical] != null)
            {
                throw new PositionOccupiedException();
            }

            var ship = new Ship(newShip);

            switch (newShip.Orientation)
            {
                case ShipOrientation.Horizontal:
                    PlaceHorizontalShip(ship);
                    break;
                case ShipOrientation.Vertical:
                    PlaceVerticalShip(ship);
                    break;
                default:
                    throw new ArgumentException($"Unknown orientation {ship.Orientation}", nameof(ship));
            }

            Ships.Add(ship);
        }

        private void PlaceHorizontalShip(Ship ship)
        {
            for (var i = ship.Horizontal; i < ship.Horizontal + ship.Size; i++)
            {
                if (Field[i, ship.Vertical] != null)
                {
                    throw new PositionOccupiedException();
                }
            }
            for (var i = ship.Horizontal; i < ship.Horizontal + ship.Size; i++)
            {
                Field[i, ship.Vertical] = ship;
            }
        }

        private void PlaceVerticalShip(Ship ship)
        {
            for (var i = ship.Vertical; i < ship.Vertical + ship.Size; i++)
            {
                if (Field[ship.Horizontal, i] != null)
                {
                    throw new PositionOccupiedException();
                }
            }
            for (var i = ship.Vertical; i < ship.Vertical + ship.Size; i++)
            {
                Field[ship.Horizontal, i] = ship;
            }
        }
    }
}