using System.Collections.Generic;
using Battleship.Models.Ships;
using Battleship.Common.Enums;

namespace Battleship.Models.Fields
{
    public class FieldStatistics
    {
        public FieldStatistics(IEnumerable<Ship> ships)
        {
            Battleship = new ShipStatistics(ShipType.Battleship, BattleShip.ShipsLimit, ships);
            Cruiser = new ShipStatistics(ShipType.Cruiser, Models.Ships.Cruiser.ShipsLimit, ships);
            Destroyer = new ShipStatistics(ShipType.Destroyer, Models.Ships.Destroyer.ShipsLimit, ships);
            Submarine = new ShipStatistics(ShipType.Submarine, Models.Ships.Submarine.ShipsLimit, ships);
        }

        public int TotalShips => Battleship.Total + Cruiser.Total + Destroyer.Total + Submarine.Total;
        public int ShipsLimit => Battleship.Limit + Cruiser.Limit + Destroyer.Limit + Submarine.Limit;
        public int ShipsLeft => ShipsLimit - TotalShips;
        
        private ShipStatistics Battleship { get; }
        private ShipStatistics Cruiser { get; }
        private ShipStatistics Destroyer { get; }
        private ShipStatistics Submarine { get; }
    }
}