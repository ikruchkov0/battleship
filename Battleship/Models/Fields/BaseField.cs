﻿using System.Linq;
using System.Collections.Generic;
using System;
using Battleship.Common.Enums;
using Battleship.Models.Ships;

namespace Battleship.Models.Fields
{
    public abstract class BaseField
    {
        private const int Size = 10;

        protected readonly IList<Ship> Ships;
        protected readonly Ship[,] Field;

        protected BaseField()
        {
            Ships = new List<Ship>();
            Field = new Ship[Size, Size];
        }

        protected BaseField(BaseField field)
        {
            Ships = field.Ships;
            Field = field.Field;
        }

        protected void ValidateCords(uint horizontal, uint vertical)
        {
            if ((horizontal < 0) || (horizontal >= Size))
            {
                throw new ArgumentException($"Wrong {nameof(horizontal)} value {horizontal}. Should be between 0 and {Size - 1}", nameof(horizontal));
            }
            if ((vertical < 0) || (vertical >= Size))
            {
                throw new ArgumentException($"Wrong {nameof(vertical)} value {vertical}. Should be between 0 and {Size - 1}", nameof(vertical));
            }
        }

        protected int GetShipsCount(ShipType type) => Ships.Count(x => x.Type == type);

        public FieldStatistics GetStatistics() => new FieldStatistics(Ships);
    }
}