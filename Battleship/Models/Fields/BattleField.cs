using Battleship.Common.Enums;

namespace Battleship.Models.Fields
{
    public class BattleField : BaseField
    {
        public BattleField(PreparationField field) : base(field) {}

        public ShotResult Shot(uint horizontal, uint vertical)
        {
            ValidateCords(horizontal, vertical);
            var ship = Field[horizontal, vertical];

            if (ship == null)
            {
                return ShotResult.Miss;
            }

            ship.Hit(horizontal, vertical);
            if (ship.Life <= 0)
            {
                Ships.Remove(ship);
                return ShotResult.Kill;
            }
            return ShotResult.Hit;
        }

        public int LiveShipsCount => Ships.Count;
    }
}