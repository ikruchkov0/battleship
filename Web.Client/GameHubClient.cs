using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Web.Client
{
    public class GameHubClient : BaseHubClient
    {
        public GameHubClient(
            Uri baseAddress,
            Func<HttpMessageHandler, HttpMessageHandler> handlerFactory,
            string token = null) : base(baseAddress, "/ws/game", handlerFactory, token)
        {
        }

        public Task<ChannelReader<GameStatus>> StreamGameUpdates(Guid gameId, CancellationToken token)
        {
            return Connection.StreamAsChannelAsync<GameStatus>("StreamGameUpdates", gameId, token);
        }
    }
}