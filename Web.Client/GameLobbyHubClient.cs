using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Web.Client
{
    public class GameLobbyHubClient : BaseHubClient
    {
        public GameLobbyHubClient(
            Uri baseAddress,
            Func<HttpMessageHandler, HttpMessageHandler> handlerFactory) 
            : base(baseAddress, "/ws/lobby", handlerFactory)
        {
        }

        public Task<ChannelReader<IEnumerable<GameInfo>>> StreamGameListUpdates(CancellationToken token)
        {
            return Connection.StreamAsChannelAsync<IEnumerable<GameInfo>>(
                "StreamGameListUpdates",
                token);
        }
    }
}