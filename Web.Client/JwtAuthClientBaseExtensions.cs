namespace Web.Client
{
    public static class JwtAuthClientBaseExtensions
    {
        public static T WithToken<T>(this T client, string bearerToken) where T : JwtAuthClientBase
        {
            client.SetBearerToken(bearerToken);
            return client;
        }
    }
}