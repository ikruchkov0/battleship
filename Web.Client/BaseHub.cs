using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Web.Client
{
    public abstract class BaseHubClient
    {

        /*public static HubConnection GetGameLobbyConnection(
            Uri baseAddress,
            Func<HttpMessageHandler, HttpMessageHandler> handlerFactory)
        {
            return GetConnection(baseAddress, "/ws/lobby", handlerFactory);
        }

        public static HubConnection GetGameConnection(
            Uri baseAddress,
            Func<HttpMessageHandler, HttpMessageHandler> handlerFactory,
            string token)
        {
            return GetConnection();
        }*/

        protected BaseHubClient(
            Uri baseAddress,
            string hubUri,
            Func<HttpMessageHandler, HttpMessageHandler> handlerFactory,
            string token = null)
        {
            Connection = new HubConnectionBuilder()
                .WithUrl(new Uri(baseAddress, hubUri),
                    o =>
                    {
                        o.HttpMessageHandlerFactory = handlerFactory;
                        if (!string.IsNullOrEmpty(token))
                        {
                            o.AccessTokenProvider = () => Task.FromResult(token);
                        }
                    })
                .Build();
        }

        protected HubConnection Connection { get; }
        
        public Task StartConnectionAsync()
        {
            return Connection.StartAsync();
        }
    }
}