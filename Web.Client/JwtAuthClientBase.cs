using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Web.Client
{
    public abstract class JwtAuthClientBase
    {
        private string _bearerToken;

        internal void SetBearerToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentException($"Wrong token value {token}", nameof(token));
            }

            _bearerToken = token;
        }
        
        protected Task<HttpRequestMessage> CreateHttpRequestMessageAsync(CancellationToken cancellationToken)
        {
            var msg = new HttpRequestMessage();
            msg.Headers.Authorization =
                new System.Net.Http.Headers.AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme,
                    _bearerToken);
            return Task.FromResult(msg);
        }
    }
}