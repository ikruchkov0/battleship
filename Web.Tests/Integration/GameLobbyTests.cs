using Web.Tests.FixtureCollection;
using Xunit;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore.Internal;
using Orleans.Runtime;
using Xunit.Abstractions;

namespace Web.Tests.Integration
{
    [Collection(WebCollection.Name)]
    public class GameLobbyTests : BaseWebTest<GameLobbyTests>
    {
        public GameLobbyTests(WebFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task StreamGameListUpdates()
        {
            var hub = CreateGameLobbyHubClient();
            await hub.StartConnectionAsync();

            var channel = await hub.StreamGameListUpdates(CancellationToken.None);
            var cancel = new CancellationDisposable();
            var observable = ChannelToObservable(channel, cancel.Token);
            var gameName = Guid.NewGuid().ToString();
            var task = observable.FirstAsync(x => x.Any(g => g.Name == gameName)).ToTask();
            
            var player = await CreatePlayerAsync();
            var gameClient = CreateGameClient(player.Token);
            
            var game = await gameClient.CreateGameAsync(gameName);

            var finalResult = await Task.WhenAny(task, Task.Delay(TimeSpan.FromSeconds(2)));

            finalResult.Should().Be(task, "expected to get task without timeout");

            var list = await task;
            list.Should().Contain(x => x.Id == game.Id);
        }
    }
}