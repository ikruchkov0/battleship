using Web.Tests.FixtureCollection;
using Xunit;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Linq;
using System.Threading.Tasks;
using Web.Client;
using System.Threading;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reactive.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.SignalR;
using Xunit.Abstractions;

namespace Web.Tests.Integration
{
    [Collection(WebCollection.Name)]
    public class GameHubTests : BaseWebTest<GameHubTests>
    {
        public GameHubTests(WebFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task StreamGameUpdates()
        {
            var player1 = await CreatePlayerAsync();
            var player2 = await CreatePlayerAsync();

            var hub = CreateGameConnection(player1.Token);
            await hub.StartConnectionAsync();
            
            var gameClient1 = CreateGameClient(player1.Token);
            var gameClient2 = CreateGameClient(player2.Token);
            var game = await gameClient1.CreateGameAsync("test");
            
            var channel = await hub.StreamGameUpdates(game.Id, CancellationToken.None);
            var cancel = new CancellationDisposable();
            var observable = ChannelToObservable(channel, cancel.Token);
            var task = observable.FirstAsync(x => x.Players.Any(p => p.Id == player2.Id)).ToTask();

            await gameClient1.PutShipAsync(game.Id,
                new Ship
                {
                    Orientation = "Vertical",
                    Horizontal = 0,
                    Vertical = 0,
                    Type = "Battleship"
                });
            
            var status = await gameClient2.JoinGameAsync(game.Id);
            
            
            var finalResult = await Task.WhenAny(task, Task.Delay(TimeSpan.FromSeconds(2)));

            finalResult.Should().Be(task, "expected to get task without timeout");

            var gameStatus = await task;
            gameStatus.Should().BeEquivalentTo(status);
        }
        
        [Fact]
        public async Task StreamGameUpdatesWrongPlayer()
        {
            var player1 = await CreatePlayerAsync();
            var player2 = await CreatePlayerAsync();
            
            var hub = CreateGameConnection(player2.Token);
            await hub.StartConnectionAsync();
            
            var gameClient1 = CreateGameClient(player1.Token);
            var game = await gameClient1.CreateGameAsync("test");
            
            var channel = await hub.StreamGameUpdates(game.Id, CancellationToken.None);

            Func<Task> act = async () => await channel.WaitToReadAsync();
            await act.Should().ThrowAsync<HubException>();
        }
    }
}