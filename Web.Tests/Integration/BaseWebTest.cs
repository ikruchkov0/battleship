using System;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Tests.Infrastructure;
using Web.Client;
using Web.Tests.FixtureCollection;
using Xunit.Abstractions;

namespace Web.Tests.Integration
{
    public abstract class BaseWebTest<T>
    {
        private readonly WebFixture _fixture;

        protected BaseWebTest(WebFixture fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            var loggerFactory = new LoggerFactory().AddXunit(output);
            Logger = loggerFactory.CreateLogger<T>();
        }

        protected PlayerClient UnauthorizedPlayerClient => _fixture.UnauthorizedPlayerClient;

        protected ILogger Logger { get; }

        protected GameClient CreateGameClient(string token)
        {
            return _fixture.GameClientFactory(token);
        }

        protected PlayerClient CreatePlayerClient(string token)
        {
            return _fixture.PlayerClientFactory(token);
        }

        protected GameLobbyHubClient CreateGameLobbyHubClient()
        {
            return new GameLobbyHubClient(
                _fixture.Server.BaseAddress,
                _ => _fixture.Server.CreateHandler()
            );
        }

        protected GameHubClient CreateGameConnection(string token)
        {
            return new GameHubClient(
                _fixture.Server.BaseAddress,
                _ => _fixture.Server.CreateHandler(),
                token
            );
        }

        protected Task<CreatedPlayer> CreatePlayerAsync()
        {
            var playerName = Guid.NewGuid().ToString();
            return UnauthorizedPlayerClient.CreatePlayerAsync(playerName);
        }

        protected IObservable<TData> ChannelToObservable<TData>(ChannelReader<TData> channel, CancellationToken token)
        {
            var subject = new Subject<TData>();
            _ = Task.Run(async () =>
            {
                while (!token.IsCancellationRequested)
                {
                    await channel.WaitToReadAsync(token);
                    var data = await channel.ReadAsync(token);
                    subject.OnNext(data);
                }
            }, token);
            return subject;
        }
    }
}