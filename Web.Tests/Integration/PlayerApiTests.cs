using System.Threading.Tasks;
using FluentAssertions;
using Web.Tests.FixtureCollection;
using Xunit;
using Xunit.Abstractions;

namespace Web.Tests.Integration
{
    [Collection(WebCollection.Name)]
    public class PlayerApiTests : BaseWebTest<PlayerApiTests>
    {
        public PlayerApiTests(WebFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task CreatePlayer()
        {
            var name = "test";
            var response = await UnauthorizedPlayerClient.CreatePlayerAsync(name);

            response.Name.Should().Be(name);
            response.IsActive.Should().BeTrue();
            response.Token.Should().NotBeNull();
            response.Token.Should().NotBeEmpty();
        }

        [Fact]
        public async Task GetStatus()
        {
            var name = "test";
            var player = await UnauthorizedPlayerClient.CreatePlayerAsync(name);
            var playerClient = CreatePlayerClient(player.Token);

            var status = await playerClient.GetPlayerStatusAsync();

            status.Name.Should().Be(name);
            status.IsActive.Should().BeTrue();
        }
    }
}
