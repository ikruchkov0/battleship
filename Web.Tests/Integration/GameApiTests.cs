using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Grains.Interfaces.Game.Models;
using Orleans.Runtime;
using Tests.Infrastructure;
using Web.Client;
using Web.Tests.FixtureCollection;
using Xunit;
using Xunit.Abstractions;

namespace Web.Tests.Integration
{
    [Collection(WebCollection.Name)]
    public class GameApiTests : BaseWebTest<GameApiTests>
    {
        public GameApiTests(WebFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }
        
        [Fact]
        public async Task CreateGame()
        {
            var player = await CreatePlayerAsync();
            var gameClient = CreateGameClient(player.Token);
            
            var response = await gameClient.CreateGameAsync("test");
            
            response.Stage.Should().Be("Created");
            response.Players.Count.Should().Be(1);
            var playerStatus = response.Players.FirstOrDefault(x => x.Id == player.Id);
            playerStatus.Should().NotBeNull();
        }

        [Fact]
        public async Task GetGames()
        {
            var player = await CreatePlayerAsync();
            var gameClient = CreateGameClient(player.Token);
            var game1 = await gameClient.CreateGameAsync("test");

            await gameClient.GetGamesAsync(); // warm cache

            var game2 = await gameClient.CreateGameAsync("test");

            var gamesList = await WaitForGames(gameClient, new[] {game1.Id, game2.Id});

            var game1FromList = gamesList.First(x => x.Id == game1.Id);
            game1FromList.Id.Should().Be(game1.Id);
            game1FromList.Name.Should().Be(game1.Name);

            var game2FromList = gamesList.First(x => x.Id == game2.Id);
            game2FromList.Id.Should().Be(game2.Id);
            game2FromList.Name.Should().Be(game2.Name);
        }

        [Fact]
        public async Task RemoveGamesFromList()
        {
            var player1 = await CreatePlayerAsync();
            var player2 = await CreatePlayerAsync();
            var gameClient1 = CreateGameClient(player1.Token);
            var gameClient2 = CreateGameClient(player2.Token);
            
            var game = await gameClient1.CreateGameAsync("test");

            await WaitForGames(gameClient1, new[] {game.Id});
            
            await gameClient2.JoinGameAsync(game.Id);

            var games = await WaitForGames(gameClient1, new[] {game.Id}, shouldNotContain: true);
            games.Select(x => x.Id).Should().NotContain(game.Id);
        }

        [Fact]
        public async Task GetGameStatus()
        {
            var player = await CreatePlayerAsync();
            var gameClient = CreateGameClient(player.Token);

            var game = await gameClient.CreateGameAsync("test");

            var status = await gameClient.GetGameStatusAsync(game.Id);
            status.Id.Should().Be(game.Id);
            status.Name.Should().Be(game.Name);
            status.Players.Select(x => x.Id).Should().Contain(player.Id);
        }

        [Fact]
        public async Task GetGameStatusForRestrictedGame()
        {
            var player1 = await CreatePlayerAsync();
            var player2 = await CreatePlayerAsync();
            var gameClient1 = CreateGameClient(player1.Token);
            var gameClient2 = CreateGameClient(player2.Token);
            
            var game = await gameClient1.CreateGameAsync("test");
            
            Func<Task> act = async () => await gameClient2.GetGameStatusAsync(game.Id);
            (await act.Should().ThrowAsync<SwaggerException>()).WithMessage("*403*");
        }

        [Fact]
        public async Task JoinGame()
        {
            var player1 = await CreatePlayerAsync();
            var player2 = await CreatePlayerAsync();
            var gameClient1 = CreateGameClient(player1.Token);
            var gameClient2 = CreateGameClient(player2.Token);
            
            var game = await gameClient1.CreateGameAsync("test");
            
            var response = await gameClient2.JoinGameAsync(game.Id);

            response.Stage.Should().Be("Created");
            response.Players.Count.Should().Be(2);
            var playerStatus = response.Players.FirstOrDefault(x => x.Id == player2.Id);
            playerStatus.Should().NotBeNull();
        }

        [Fact]
        public async Task PutShip()
        {
            var player = await CreatePlayerAsync();
            var gameClient = CreateGameClient(player.Token);
            var game = await gameClient.CreateGameAsync("test");

            var response = await gameClient.PutShipAsync(game.Id, ToShipModel(TestShip.BattleShip));

            response.Stage.Should().Be("Created");
            response.Players.Count.Should().Be(1);
            var playerStatus = response.Players.FirstOrDefault(x => x.Id == player.Id);
            playerStatus.Should().NotBeNull();
        }
        
        [Fact]
        public async Task Shot()
        {
            var player1 = await CreatePlayerAsync();
            var player2 = await CreatePlayerAsync();
            var gameClient1 = CreateGameClient(player1.Token);
            var gameClient2 = CreateGameClient(player2.Token);
            var game = await gameClient1.CreateGameAsync("test");

            await gameClient2.JoinGameAsync(game.Id);

            await FillFieldAsync(gameClient1, game.Id);
            await FillFieldAsync(gameClient2, game.Id);

            var status = await gameClient1.GetGameStatusAsync(game.Id);

            var gameClientToShot = status.PlayerTurn.Id == player1.Id ? gameClient1 : gameClient2;

            var shotResponse = await gameClientToShot.ShotAsync(game.Id, new Shot {Horizontal = 1, Vertical = 2});

            shotResponse.Stage.Should().Be("Started");
            shotResponse.ShotHistory.Should().HaveCount(1);
            var shotResult = shotResponse.ShotHistory.First();
            shotResult.Horizontal.Should().Be(1);
            shotResult.Vertical.Should().Be(2);
            shotResult.IsHit.Should().BeTrue();
        }

        private async Task<IList<GameInfo>> WaitForGames(GameClient gameClient, Guid[] gameIds, bool shouldNotContain = false)
        {
            Exception lastEx = null;
            for (var i = 0; i < 5; i++)
            {
                var result = await gameClient.GetGamesAsync();
                var ids = result.Select(x => x.Id).ToArray();
                try
                {
                    if (shouldNotContain)
                    {
                        ids.Should().NotContain(gameIds);
                        return result.ToList();
                    }

                    ids.Should().Contain(gameIds);
                    return result.ToList();
                }
                catch (Exception ex)
                {
                    Logger.Info("Received exception {0}", ex);
                    lastEx = ex;
                }

                await Task.Delay(TimeSpan.FromMilliseconds(100));
            }

            throw new TimeoutException("Unable to get correct list of games.", lastEx);
        }

        private Ship ToShipModel(IShip ship)
        {
            return new Ship
            {
                Horizontal = (int) ship.Horizontal,
                Vertical = (int) ship.Vertical,
                Type = ship.Type.ToString(),
                Orientation = ship.Orientation.ToString(),
            };
        }
        
        private async Task FillFieldAsync(GameClient gameClient, Guid gameId)
        {
            foreach (var ship in TestShip.ShipsToFillField)
            {
                var model = ToShipModel(ship);
                await gameClient.PutShipAsync(gameId, model);
            }
        }
    }
}