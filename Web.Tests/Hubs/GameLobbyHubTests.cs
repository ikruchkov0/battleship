using System.Threading.Tasks;
using Web.Hubs;
using Xunit;
using FakeItEasy;
using Web.ViewModels;
using System.Reactive.Subjects;
using System.Collections.Generic;
using Grains.Interfaces.GameLobby;
using System;
using System.Reactive.Disposables;
using FluentAssertions;
using System.Threading;
using Web.Services.Gameplay.Interfaces;

namespace Web.Tests.Hubs
{
    public class GameLobbyHubTests
    {
        [Fact]
        public void GetStream()
        {
            var testSubject = new Subject<IEnumerable<IGameInfo>>();
            var testInfo = A.Fake<IGameInfo>();
            A.CallTo(() => testInfo.Id).Returns(Guid.NewGuid());

            var stream = A.Fake<IGameListStream>();
            A.CallTo(() => stream.AsObservable()).Returns(testSubject);

            var mapper = Mappings.Create();
            var hub = new GameLobbyHub(stream, mapper);

            var channel = hub.StreamGameListUpdates(CancellationToken.None);
            var cancel = new CancellationDisposable();
            var task = Task.Run(async () =>
            {
                await channel.WaitToReadAsync(cancel.Token);
                return await channel.ReadAsync(cancel.Token);
            }, cancel.Token);

            testSubject.OnNext(new [] { testInfo });

            task.Wait(TimeSpan.FromSeconds(2));

            var list = task.Result;

            list.Should().Contain(x => x.Id == testInfo.Id);
        }

        [Fact]
        public async Task GetStreamCancel()
        {
            var testSubject = new Subject<IEnumerable<IGameInfo>>();
            var testInfo = A.Fake<IGameInfo>();
            A.CallTo(() => testInfo.Id).Returns(Guid.NewGuid());

            var stream = A.Fake<IGameListStream>();
            A.CallTo(() => stream.AsObservable()).Returns(testSubject);

            var mapper = Mappings.Create();
            var cancelStream = new CancellationTokenSource();
            var hub = new GameLobbyHub(stream, mapper);

            var channel = hub.StreamGameListUpdates(cancelStream.Token);
            
            var cancel = new CancellationDisposable();
            var task = Task.Run(async () =>
            {
                await channel.WaitToReadAsync(cancel.Token);
                return await channel.ReadAsync(cancel.Token);
            }, cancel.Token);

            cancelStream.Cancel();

            testSubject.OnNext(new [] { testInfo });

            var delayTask = Task.Delay(TimeSpan.FromSeconds(1));
            var resultTask = await Task.WhenAny(task, delayTask);

            resultTask.Should().Be(delayTask);
        }
    }
}