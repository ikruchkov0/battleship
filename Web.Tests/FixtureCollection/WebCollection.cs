using Xunit;

namespace Web.Tests.FixtureCollection
{
    [CollectionDefinition(Name)]
    public class WebCollection : ICollectionFixture<WebFixture>
    {
        public const string Name = "WebCollection";
    }
}
