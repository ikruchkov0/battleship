using System;
using Microsoft.AspNetCore.TestHost;
using Orleans.TestingHost;
using Tests.Infrastructure;
using Web.Client;

namespace Web.Tests.FixtureCollection
{
    public delegate GameClient GameClientFactory(string token);
    public delegate PlayerClient PlayerClientFactory(string token);
    
    public class WebFixture : IDisposable
    {
        private readonly TestCluster _cluster;
        
        public WebFixture()
        {
            _cluster = StartCluster();
            var factory = new CustomWebApplicationFactory(_cluster.Client);
            var client = factory.CreateClient();
            Server = factory.Server;
            UnauthorizedPlayerClient = new PlayerClient(client);
            GameClientFactory = token => new GameClient(client).WithToken(token);
            PlayerClientFactory = token => new PlayerClient(client).WithToken(token);
        }
        
        public PlayerClient UnauthorizedPlayerClient { get; }
        public GameClientFactory GameClientFactory { get; }
        public PlayerClientFactory PlayerClientFactory { get; }
        public TestServer Server { get; }

        public void Dispose()
        {
            if (_cluster != null)
            {
                _cluster.StopAllSilos();
            }
        }

        private static TestCluster StartCluster()
        {
            var cluster = TestClusterFactory.Create();
            cluster.Deploy();
            return cluster;
        }
    }
}
