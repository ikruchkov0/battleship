using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Orleans;

namespace Web.Tests.FixtureCollection
{
    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>
    {
        private readonly IClusterClient _clusterClient;
        public CustomWebApplicationFactory(IClusterClient clusterClient)
        {
            _clusterClient = clusterClient;
        }

        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder(new string[0])
                .UseStartup<Startup>()
                .ConfigureTestServices(services => {
                    services.AddSingleton(_clusterClient);
                });
        }
    }
}
