using Battleship.Common.Enums;

namespace Battleship.Common.Interfaces
{
    public interface IShipData
    {
        ShipOrientation Orientation { get; }
        ShipType Type { get; }
        int Limit { get; }
        int Size { get; }
        uint Horizontal { get; }
        uint Vertical { get; }
    }
}