using System;

namespace Battleship.Common.Exceptions
{
    public class PlayerException : Exception
    {
        public PlayerException(string message) : base(message)
        {
        }
    }
}