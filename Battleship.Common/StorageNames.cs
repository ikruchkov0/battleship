namespace Battleship.Common
{
    public static class StorageNames
    {
        public const string TransactionStoreName = "TransactionStore";
        public const string GameStateName = "game";
        public const string GameLobbyStateName = "game-lobby";
        public const string PlayerStateName = "player";
        public const string StreamProviderName = "SMSProvider";
        public const string GameListStream = "GameListStream";
        public const string GameStream = "GameStream";
    }
}