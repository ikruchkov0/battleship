namespace Battleship.Common.Enums
{
    public enum ShotResult
    {
        Hit, Miss, Kill
    }
}