namespace Battleship.Common.Enums
{
    public enum GameStage
    {
        None, Created, Started, Finished
    }
}
