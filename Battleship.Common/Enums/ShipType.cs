namespace Battleship.Common.Enums
{
    public enum ShipType
    {
        Battleship, Cruiser, Destroyer, Submarine
    }
}