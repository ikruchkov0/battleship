namespace Battleship.Common.Enums
{
    public enum ShipOrientation
    {
        Vertical, Horizontal
    }
}