using Grains.Game;
using Grains.Interfaces.Game;
using Orleans.Hosting;
using Orleans.TestingHost;

namespace Tests.Infrastructure.Configuration
{
    internal class TestSiloConfigurations : ISiloBuilderConfigurator
    {
        public void Configure(ISiloHostBuilder hostBuilder)
        {
            hostBuilder.AddSiloConfiguration(typeof(GameGrain).Assembly, typeof(IGameGrain).Assembly);
        }
    }
}
