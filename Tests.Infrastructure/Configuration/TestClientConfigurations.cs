using Grains.Game;
using Grains.Interfaces.Game;
using Microsoft.Extensions.Configuration;
using Orleans;
using Orleans.TestingHost;

namespace Tests.Infrastructure.Configuration
{
    internal class TestClientConfigurations : IClientBuilderConfigurator
    {
        public void Configure(IConfiguration configuration, IClientBuilder clientBuilder)
        {
            clientBuilder.AddClientConfiguration(typeof(GameGrain).Assembly, typeof(IGameGrain).Assembly);
        }
    }
}
