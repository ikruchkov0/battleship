﻿using System.Reflection;
using Battleship.Common;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Hosting;

namespace Tests.Infrastructure.Configuration
{
    internal static class ConfigurationExtension
    {
        public static ISiloHostBuilder AddSiloConfiguration(this ISiloHostBuilder hostBuilder, params Assembly[] grainsAssemblies)
        {
            var result = hostBuilder
                .ConfigureServices((context, services) =>
                {
                    services.AddLogging(builder => builder
                        .AddConsole()
                        .AddFilter(level => level >= LogLevel.Error)
                    );
                })
                .AddSimpleMessageStreamProvider(StorageNames.StreamProviderName)
                .AddMemoryGrainStorage("PubSubStore")
                .AddMemoryGrainStorage(StorageNames.TransactionStoreName)
                .UseTransactions();
            
            foreach (var assembly in grainsAssemblies)
            {
                result = result
                    .ConfigureApplicationParts(m => m.AddApplicationPart(assembly).WithReferences());
            }

            return result;
        }

        public static IClientBuilder AddClientConfiguration(this IClientBuilder clientBuilder, params Assembly[] grainsAssemblies)
        {
            var result = clientBuilder
                .ConfigureServices((context, services) =>
                {
                    services.AddLogging(builder => builder
                        .AddConsole()
                        .AddFilter(level => level >= LogLevel.Error)
                    );
                })
                .AddSimpleMessageStreamProvider(StorageNames.StreamProviderName);
            
            foreach (var assembly in grainsAssemblies)
            {
                result = result
                    .ConfigureApplicationParts(m => m.AddApplicationPart(assembly).WithReferences());
            }

            return result;
        }
    }
}
