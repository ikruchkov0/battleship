﻿using Orleans.TestingHost;
using Tests.Infrastructure.Configuration;

namespace Tests.Infrastructure
{
    public static class TestClusterFactory
    {
        public static TestCluster Create()
        {
            var builder = new TestClusterBuilder(1);
            builder.AddClientBuilderConfigurator<TestClientConfigurations>();
            builder.AddSiloBuilderConfigurator<TestSiloConfigurations>();
            return builder.Build();
        }
    }
}
