using System;
using System.Collections.Generic;
using System.Linq;
using Battleship.Common.Enums;
using Grains.Interfaces.Game.Models;

namespace Tests.Infrastructure
{
    public class TestShip : IShip
    {
        public TestShip(ShipType type, ShipOrientation orientation, uint horizontal, uint vertical)
        {
            Orientation = orientation;
            Type = type;
            Horizontal = horizontal;
            Vertical = vertical;

        }
        public ShipOrientation Orientation { get; }
        public ShipType Type { get; }
        public uint Horizontal { get; }
        public uint Vertical { get; }
        public int ShipSize => GetShipSize(Type);

        public IEnumerable<(uint, uint)> GetShipSegmentsCoords()
        {
            for (uint i = 0; i < ShipSize; i++)
            {
                switch (Orientation)
                {
                    case ShipOrientation.Horizontal:
                        yield return (Horizontal + i, Vertical);
                        break;
                    case ShipOrientation.Vertical:
                        yield return (Horizontal, Vertical + i); 
                        break;
                }
            }
        }

        public static readonly TestShip BattleShip = new TestShip(ShipType.Battleship, ShipOrientation.Vertical, 0, 0);

        public static readonly IEnumerable<TestShip> Cruisers = new []
        {
            new TestShip(ShipType.Cruiser, ShipOrientation.Vertical, 1, 0),
            new TestShip(ShipType.Cruiser, ShipOrientation.Vertical, 2, 0),
        };

        public static readonly IEnumerable<TestShip> Destroyers = new []
        {
            new TestShip(ShipType.Destroyer, ShipOrientation.Vertical, 3, 0),
            new TestShip(ShipType.Destroyer, ShipOrientation.Vertical, 4, 0),
            new TestShip(ShipType.Destroyer, ShipOrientation.Vertical, 5, 0),
        };

        public static readonly IEnumerable<TestShip> Submarines = new []
        {
            new TestShip(ShipType.Submarine, ShipOrientation.Vertical, 0, 9),
            new TestShip(ShipType.Submarine, ShipOrientation.Vertical, 1, 9),
            new TestShip(ShipType.Submarine, ShipOrientation.Vertical, 2, 9),
            new TestShip(ShipType.Submarine, ShipOrientation.Vertical, 3, 9),

        };

        public static IEnumerable<TestShip> ShipsToFillField =>
            new [] { BattleShip }.AsEnumerable()
            .Concat(Cruisers)
            .Concat(Destroyers)
            .Concat(Submarines);

        private static int GetShipSize(ShipType type)
        {
            switch (type)
            {
                case ShipType.Battleship:
                    return Battleship.Models.Ships.BattleShip.ShipSize;
                case ShipType.Destroyer:
                    return Battleship.Models.Ships.Destroyer.ShipSize;
                case ShipType.Cruiser:
                    return Battleship.Models.Ships.Cruiser.ShipSize;
                case ShipType.Submarine:
                    return Battleship.Models.Ships.Submarine.ShipSize;
                default:
                    throw new Exception($"Wrong ship type {type}");
            }
        }
    }
}