﻿using System;
using System.IO;
using System.Threading.Tasks;
using NSwag;
using NSwag.CodeGeneration.CSharp;

namespace Web.Generate
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var swaggerUrl = "http://localhost:5000/swagger/v1/swagger.json";
            var targetFilePath = "../Web.Client/WebClient.cs";
            
            var document = await SwaggerDocument.FromUrlAsync(swaggerUrl);

            var settings = new SwaggerToCSharpClientGeneratorSettings
            {
                ClientBaseClass = "JwtAuthClientBase",
                UseHttpRequestMessageCreationMethod = true,
                CSharpGeneratorSettings =
                {
                    Namespace = "Web.Client"
                }
            };

            var generator = new SwaggerToCSharpClientGenerator(document, settings);
            var code = generator.GenerateFile();
            Console.WriteLine(code);
            await File.WriteAllTextAsync(targetFilePath, code);
            Console.WriteLine("Written");
        }
    }
}
