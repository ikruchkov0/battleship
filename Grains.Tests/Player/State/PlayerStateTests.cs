using System;
using Battleship.Common.Exceptions;
using FluentAssertions;
using Grains.Player.State;
using Xunit;
using Battleship.Common.Enums;

namespace Grains.Tests.Player.State
{
    public class PlayerStateTests
    {
        private readonly PlayerState _state;
        public PlayerStateTests()
        {
            _state = new PlayerState();
        }

        [Fact]
        public void Create()
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            _state.Create(playerId, name);
            var status = _state.GetStatus();

            status.Id.Should().Be(playerId);
            status.Name.Should().Be(name);
            status.IsActive.Should().BeTrue();
        }

        [Fact]
        public void CreateForActive()
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            _state.Create(playerId, name);
            _state.Invoking(s => s.Create(playerId, name)).Should().Throw<PlayerException>();
        }

        [Fact]
        public void JoinGame()
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            var gameId = Guid.NewGuid();
            _state.Create(playerId, name);
            _state.JoinGame(gameId);
            var status = _state.GetStatus();

            status.ActiveGames.Should().Contain(x => x == gameId);
        }

        [Fact]
        public void JoinJoinedGame()
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            var gameId = Guid.NewGuid();
            _state.Create(playerId, name);
            _state.JoinGame(gameId);
            _state.Invoking(s => s.JoinGame(gameId)).Should().Throw<PlayerException>();
        }

        [Theory]
        [InlineData(true, 1, 0)]
        [InlineData(false, 0, 1)]
        public void EndGame(bool hasWon, int expectedGamesWon, int expectedGamesLost)
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            var gameId = Guid.NewGuid();
            _state.Create(playerId, name);
            _state.JoinGame(gameId);
            _state.EndGame(gameId, hasWon);
            var status = _state.GetStatus();

            status.ActiveGames.Should().BeEmpty();
            status.Statistics.GamesWon.Should().Be(expectedGamesWon);
            status.Statistics.GamesLost.Should().Be(expectedGamesLost);
        }

        [Fact]
        public void EndUnknownGame()
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            var gameId = Guid.NewGuid();
            var unknownGameId = Guid.NewGuid();
            _state.Create(playerId, name);
            _state.JoinGame(gameId);

            _state.Invoking(s => s.EndGame(unknownGameId, false)).Should().Throw<PlayerException>();
        }

        [Theory]
        [InlineData(new [] { ShotResult.Hit }, 0, 0)]
        [InlineData(new [] { ShotResult.Hit, ShotResult.Hit }, 0, 0)]
        [InlineData(new [] { ShotResult.Hit, ShotResult.Kill }, 0, 1)]
        [InlineData(new [] { ShotResult.Miss, ShotResult.Kill }, 1, 1)]
        [InlineData(new [] { ShotResult.Miss, ShotResult.Hit }, 1, 0)]
        public void AddShot(ShotResult[] shotResults, int expectedShotsMiss, int expectedShipsKilled)
        {
            var playerId = Guid.NewGuid();
            var name = "test";
            var gameId = Guid.NewGuid();
            _state.Create(playerId, name);
            _state.JoinGame(gameId);
            foreach (var result in shotResults)
            {
                _state.AddShot(gameId, result);
            }

            var status = _state.GetStatus();

            status.Statistics.ShotsFired.Should().Be(shotResults.Length);
            status.Statistics.ShotsMissed.Should().Be(expectedShotsMiss);
            status.Statistics.ShipsKilled.Should().Be(expectedShipsKilled);
        }
    }
}