using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Reactive.Subjects;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using Battleship.Common;
using Battleship.Common.Enums;
using FluentAssertions;
using Grains.Interfaces.Game;
using Grains.Interfaces.Game.Messages;
using Grains.Interfaces.Game.Models;
using Grains.Interfaces.GameLobby.Messages;
using Grains.Interfaces.Player;
using Grains.Tests.FixtureCollection;
using Microsoft.Extensions.Logging;
using Orleans.Runtime;
using Orleans.Streams;
using Orleans.TestingHost;
using Tests.Infrastructure;
using Xunit;
using Xunit.Abstractions;

namespace Grains.Tests.Integration
{
    [Collection(ClusterCollection.Name)]
    public class GameplayTests
    {
        private readonly TestCluster _cluster;
        private readonly ILogger _logger;

        public GameplayTests(ClusterFixture fixture, ITestOutputHelper output)
        {
            _cluster = fixture.Cluster;
            var loggerFactory = new LoggerFactory().AddXunit(output);
            _logger = loggerFactory.CreateLogger<GameplayTests>();
        }

        [Fact]
        public async Task CreateGame()
        {
            var gameId = Guid.NewGuid();
            var lobbyId = GrainNames.MainLobby;
            var streamProvider = _cluster.Client.GetStreamProvider(StorageNames.StreamProviderName);
            var stream = streamProvider.GetStream<IGameListUpdated>(lobbyId, StorageNames.GameListStream);
            var cancellationToken = new CancellationDisposable();

            var observable = await ToObservable(stream, cancellationToken.Token);

            var gameListUpdateTask = observable.FirstAsync(data => data.GameList.Any(x => x.Id == gameId)).ToTask();

            var game = _cluster.Client.GetGrain<IGameGrain>(gameId);
            await game.CreateAsync("Hello");
            _logger.Info("Game created");
            var status = await game.GetStatusAsync();

            status.PlayerTurn.Should().BeNull();
            status.Stage.Should().Be(GameStage.Created);
            status.Players.Should().BeEmpty();

            var list = await AwaitWithTimeout(gameListUpdateTask, TimeSpan.FromSeconds(2));
            list.GameList.Should().Contain(x => x.Id == gameId);
        }

        [Fact]
        public async Task JoinGame()
        {
            var playerId = Guid.NewGuid();
            var gameId = Guid.NewGuid();

            var streamProvider = _cluster.Client.GetStreamProvider(StorageNames.StreamProviderName);
            var stream = streamProvider.GetStream<IGameStatusUpdated>(gameId, StorageNames.GameStream);
            var cancellationToken = new CancellationDisposable();

            var observable = await ToObservable(stream, cancellationToken.Token);

            var gameUpdateTask = observable
                .FirstAsync(data => data.NewStatus.Players.Any(x => x.Id == playerId))
                .ToTask();

            var player = _cluster.Client.GetGrain<IPlayerGrain>(playerId);
            await player.CreateAsync("test");
            var game = _cluster.Client.GetGrain<IGameGrain>(gameId);
            await game.CreateAsync("Hello");
            await game.JoinPlayer(playerId);
            var status = await game.GetStatusAsync();
            var playerStatus = await player.GetStatusAsync();

            status.PlayerTurn.Should().BeNull();
            status.Players.Should().NotBeNull();
            status.Players.Should().NotBeEmpty();

            var gamePlayerStatus = status.Players.First(x => x.Id == playerId);
            gamePlayerStatus.FieldStatus.Should().NotBeNull();
            gamePlayerStatus.FieldStatus.TotalShips.Should().Be(0);

            playerStatus.ActiveGames.Should().HaveCount(1);
            var activeGame = playerStatus.ActiveGames.First();
            activeGame.Should().Be(gameId);

            var gameUpdate = await gameUpdateTask;
            gameUpdate.NewStatus.Should().BeEquivalentTo(status);
        }

        [Fact]
        public async Task PutShip()
        {
            var playerId = Guid.NewGuid();
            var gameId = Guid.NewGuid();
            var player = _cluster.Client.GetGrain<IPlayerGrain>(playerId);
            await player.CreateAsync("test");
            var game = _cluster.Client.GetGrain<IGameGrain>(gameId);
            await game.CreateAsync("Hello");
            await game.JoinPlayer(playerId);
            var status = await game.PutShip(playerId, new TestShip(ShipType.Submarine, ShipOrientation.Vertical, 1, 1));

            status.PlayerTurn.Should().BeNull();

            var gamePlayerStatus = status.Players.First(x => x.Id == playerId);
            gamePlayerStatus.FieldStatus.Should().NotBeNull();
            gamePlayerStatus.FieldStatus.TotalShips.Should().Be(1);
            gamePlayerStatus.FieldStatus.ShipsLimit.Should().Be(10);
            gamePlayerStatus.FieldStatus.ShipsLeft.Should().Be(9);
        }

        [Fact]
        public async Task StartGame()
        {
            var playerId1 = Guid.NewGuid();
            var playerId2 = Guid.NewGuid();
            var player1 = _cluster.Client.GetGrain<IPlayerGrain>(playerId1);
            await player1.CreateAsync("test");
            var player2 = _cluster.Client.GetGrain<IPlayerGrain>(playerId2);
            await player2.CreateAsync("test");
            var gameId = Guid.NewGuid();
            var game = _cluster.Client.GetGrain<IGameGrain>(gameId);
            await game.CreateAsync("Hello");
            await game.JoinPlayer(playerId1);
            await game.JoinPlayer(playerId2);

            await FillFieldAsync(playerId1, game);
            await FillFieldAsync(playerId2, game);

            var status = await game.GetStatusAsync();

            status.Stage.Should().Be(GameStage.Started);

            status.PlayerTurn.Should().NotBeNull();
            status.PlayerTurn.Should().Match<IPlayerStatus>(x => x.Id == playerId1 || x.Id == playerId2);

            status.ShotHistory.Should().NotBeNull();
            status.ShotHistory.Should().BeEmpty();

            var gamePlayerStatus1 = status.Players.First(x => x.Id == playerId1);
            var gamePlayerStatus2 = status.Players.First(x => x.Id == playerId2);

            gamePlayerStatus1.FieldStatus.TotalShips.Should().Be(10);
            gamePlayerStatus2.FieldStatus.TotalShips.Should().Be(10);

            gamePlayerStatus1.FieldStatus.ShipsLeft.Should().Be(0);
            gamePlayerStatus2.FieldStatus.ShipsLeft.Should().Be(0);

            status.PlayerTurn.Should().NotBeNull();
        }

        [Fact]
        public async Task HitShip()
        {
            var playerId1 = Guid.NewGuid();
            var playerId2 = Guid.NewGuid();
            var player1 = _cluster.Client.GetGrain<IPlayerGrain>(playerId1);
            await player1.CreateAsync("test");
            var player2 = _cluster.Client.GetGrain<IPlayerGrain>(playerId2);
            await player2.CreateAsync("test");
            var gameId = Guid.NewGuid();
            var game = _cluster.Client.GetGrain<IGameGrain>(gameId);
            await game.CreateAsync("Hello");
            await game.JoinPlayer(playerId1);
            await game.JoinPlayer(playerId2);

            await FillFieldAsync(playerId1, game);
            await FillFieldAsync(playerId2, game);

            var status = await game.GetStatusAsync();

            var playerId = status.PlayerTurn.Id;
            var playerIdToHit = status.PlayerToHit.Id;

            var player = _cluster.Client.GetGrain<IPlayerGrain>(playerId);

            var submarineToHit = TestShip.Submarines.First();
            await game.Shot(playerId, submarineToHit.Horizontal, submarineToHit.Vertical); // hit submarine

            var statusAfterHit = await game.GetStatusAsync();
            var playerStatus = await player.GetStatusAsync();

            statusAfterHit.PlayerTurn.Id.Should().Be(playerIdToHit);
            statusAfterHit.ShotHistory.Should().HaveCount(1);
            var shot = statusAfterHit.ShotHistory.First();
            shot.PlayerId.Should().Be(playerId);
            shot.PlayerToHitId.Should().Be(playerIdToHit);
            shot.Horizontal.Should().Be(submarineToHit.Horizontal);
            shot.Vertical.Should().Be(submarineToHit.Vertical);
            shot.IsKill.Should().BeTrue();

            var gamePlayerStatus = statusAfterHit.Players.First(x => x.Id == playerId);
            var gamePlayerStatusToHit = statusAfterHit.Players.First(x => x.Id == playerIdToHit);

            gamePlayerStatus.FieldStatus.TotalShips.Should().Be(10);

            gamePlayerStatusToHit.FieldStatus.TotalShips.Should().Be(9);

            playerStatus.Statistics.ShotsFired.Should().Be(1);
            playerStatus.Statistics.ShotsMissed.Should().Be(0);
            playerStatus.Statistics.ShipsKilled.Should().Be(1);
        }

        [Fact]
        public async Task WinGame()
        {
            var playerId1 = Guid.NewGuid();
            var playerId2 = Guid.NewGuid();
            var player1 = _cluster.Client.GetGrain<IPlayerGrain>(playerId1);
            await player1.CreateAsync("test");
            var player2 = _cluster.Client.GetGrain<IPlayerGrain>(playerId2);
            await player2.CreateAsync("test");
            var gameId = Guid.NewGuid();
            var game = _cluster.Client.GetGrain<IGameGrain>(gameId);
            await game.CreateAsync("Hello");
            await game.JoinPlayer(playerId1);
            await game.JoinPlayer(playerId2);

            await FillFieldAsync(playerId1, game);
            await FillFieldAsync(playerId2, game);
            await KillAllShips(playerId2, game);

            var status = await game.GetStatusAsync();
            var player1Status = await player1.GetStatusAsync();
            var player2Status = await player2.GetStatusAsync();

            status.Stage.Should().Be(GameStage.Finished);
            status.Winner.Should().NotBeNull();

            status.Winner.Id.Should().Be(playerId1);

            player1Status.ActiveGames.Should().BeEmpty();
            player2Status.ActiveGames.Should().BeEmpty();

            player1Status.Statistics.GamesWon.Should().Be(1);
            player2Status.Statistics.GamesWon.Should().Be(0);

            player1Status.Statistics.GamesLost.Should().Be(0);
            player2Status.Statistics.GamesLost.Should().Be(1);
        }

        private async Task FillFieldAsync(Guid playerId, IGameGrain game)
        {
            foreach (var ship in TestShip.ShipsToFillField)
            {
                await game.PutShip(playerId, ship);
            }
        }

        private async Task HitMissAsync(Guid playerToHit, IGameGrain game, uint x, uint y)
        {
            var state = await game.GetStatusAsync();
            if (state.PlayerTurn.Id == playerToHit)
            {
                await game.Shot(state.PlayerTurn.Id, 0, 8); // miss
            }

            state = await game.GetStatusAsync();
            await game.Shot(state.PlayerTurn.Id, x, y);
        }

        private async Task KillAllShips(Guid playerToHit, IGameGrain game)
        {
            foreach (var ship in TestShip.ShipsToFillField)
            {
                foreach (var (x, y) in ship.GetShipSegmentsCoords())
                {
                    await HitMissAsync(playerToHit, game, x, y);
                }
            }
        }

        private async Task<IObservable<T>> ToObservable<T>(IAsyncStream<T> stream, CancellationToken cancellationToken)
        {
            var subject = new Subject<T>();

            var subscriptionHandle = await stream.SubscribeAsync((data, token) =>
            {
                _logger.Info("Data received");
                subject.OnNext(data);
                return Task.CompletedTask;
            });

            _logger.Info("Subscribed");

            cancellationToken.Register(() => subscriptionHandle.UnsubscribeAsync().GetAwaiter().GetResult());

            return subject;
        }

        private async Task<T> AwaitWithTimeout<T>(Task<T> task, TimeSpan timeout)
        {
            var resultTask = await Task.WhenAny(task, Task.Delay(timeout));
            if (resultTask == task)
            {
                return await task;
            }

            throw new TimeoutException("Error while waiting for task");
        }
    }
}
