using System;
using Orleans.TestingHost;
using Tests.Infrastructure;

namespace Grains.Tests.FixtureCollection
{
    public class ClusterFixture : IDisposable
    {
        public ClusterFixture()
        {
            var cluster = TestClusterFactory.Create();
            cluster.Deploy();
            Cluster = cluster;
        }

        public void Dispose()
        {
            Cluster.StopAllSilos();
        }

        public TestCluster Cluster { get; }
    }
}
