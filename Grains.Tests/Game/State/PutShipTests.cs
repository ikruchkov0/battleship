using System;
using System.Linq;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using FluentAssertions;
using Grains.Game.State;
using Grains.Interfaces.Game.Models;
using Tests.Infrastructure;
using Xunit;

namespace Grains.Tests.Game.State
{
    public class PutShipTests
    {
        private readonly GameState _state;
        private readonly Guid _playerId;
        private readonly Guid _otherPlayerId;

        public PutShipTests()
        {
            _state = new GameState();
            _state.Initialize(Guid.NewGuid(), "test");
            _playerId = Guid.NewGuid();
            _state.JoinPlayer(_playerId);
            _otherPlayerId = Guid.NewGuid();
            _state.JoinPlayer(_otherPlayerId);
        }

        [Fact]
        public void PutShipPositive()
        {
            _state.PutShip(_playerId, new TestShip(ShipType.Submarine, ShipOrientation.Vertical, 1, 1));
            var status = _state.GetStatus();

            status.PlayerTurn.Should().BeNull();

            var player = status.Players.First(x => x.Id == _playerId);
            player.FieldStatus.Should().NotBeNull();
            player.FieldStatus.TotalShips.Should().Be(1);
            player.FieldStatus.ShipsLimit.Should().Be(10);
            player.FieldStatus.ShipsLeft.Should().Be(9);
        }

        [Fact]
        public void PutShipWrongPlayer()
        {
            _state.Invoking(x => x.PutShip(Guid.NewGuid(), TestShip.Submarines.First())).Should().Throw<GameException>();
        }

        [Fact]
        public void StartGame()
        {
            FillField(_playerId);
            FillField(_otherPlayerId);
            var status = _state.GetStatus();

            status.Stage.Should().Be(GameStage.Started);

            status.PlayerTurn.Should().NotBeNull();
            status.PlayerTurn.Should().Match<IPlayerStatus>(x => x.Id == _playerId || x.Id == _otherPlayerId);

            status.ShotHistory.Should().NotBeNull();
            status.ShotHistory.Should().BeEmpty();

            var player1 = status.Players.First(x => x.Id == _playerId);
            var player2 = status.Players.First(x => x.Id == _otherPlayerId);

            player1.FieldStatus.TotalShips.Should().Be(10);
            player2.FieldStatus.TotalShips.Should().Be(10);

            player1.FieldStatus.ShipsLeft.Should().Be(0);
            player2.FieldStatus.ShipsLeft.Should().Be(0);
        }

        private void FillField(Guid playerId)
        {
            foreach (var ship in TestShip.ShipsToFillField) 
            {
                _state.PutShip(playerId, ship);
            }
        }
    }
}