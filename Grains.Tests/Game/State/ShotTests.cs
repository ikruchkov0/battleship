using System;
using System.Linq;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using FluentAssertions;
using Grains.Game.State;
using Tests.Infrastructure;
using Xunit;

namespace Grains.Tests.Game.State
{
    public class ShotTests
    {
        private readonly GameState _state;
        private readonly Guid _playerId;
        private readonly Guid _otherPlayerId;

        public ShotTests()
        {
            _state = new GameState();
            _state.Initialize(Guid.NewGuid(), "test");
            _playerId = Guid.NewGuid();
            _state.JoinPlayer(_playerId);
            _otherPlayerId = Guid.NewGuid();
            _state.JoinPlayer(_otherPlayerId);

            foreach(var p in new [] { _playerId, _otherPlayerId })
            {
                foreach (var ship in TestShip.ShipsToFillField) 
                {
                    _state.PutShip(p, ship);
                }
            }
        }

        [Fact]
        public void ShotAndHit()
        {
            var x = TestShip.BattleShip.Horizontal;
            var y = TestShip.BattleShip.Vertical;
            var playerTurnId = PlayerTurnId;
            var playerToHitId = PlayerToHitId;
            _state.Shot(playerTurnId, x, y);
            var status = _state.GetStatus();

            status.ShotHistory.Should().HaveCount(1);
            var entry = status.ShotHistory.First();
            entry.PlayerId.Should().Be(playerTurnId);
            entry.PlayerToHitId.Should().Be(playerToHitId);
            entry.Horizontal.Should().Be(x);
            entry.Vertical.Should().Be(y);
            entry.IsKill.Should().BeFalse();
            entry.IsHit.Should().BeTrue();
            entry.IsMiss.Should().BeFalse();

            status.PlayerTurn.Id.Should().Be(playerToHitId);
            status.PlayerToHit.Id.Should().Be(playerTurnId);

            status.PlayerTurn.FieldStatus.TotalShips.Should().Be(TestShip.ShipsToFillField.Count());
        }

        [Fact]
        public void ShotAndMiss()
        {
            var x = 4u;
            var y = 9u;
            var playerTurnId = PlayerTurnId;
            var playerToHitId = PlayerToHitId;
            _state.Shot(playerTurnId, x, y);
            var status = _state.GetStatus();

            status.ShotHistory.Should().HaveCount(1);
            var entry = status.ShotHistory.First();
            entry.PlayerId.Should().Be(playerTurnId);
            entry.PlayerToHitId.Should().Be(playerToHitId);
            entry.Horizontal.Should().Be(x);
            entry.Vertical.Should().Be(y);
            entry.IsKill.Should().BeFalse();
            entry.IsHit.Should().BeFalse();
            entry.IsMiss.Should().BeTrue();

            status.PlayerTurn.FieldStatus.TotalShips.Should().Be(TestShip.ShipsToFillField.Count());
        }

        [Fact]
        public void ShotAndKill()
        {
            var x = TestShip.Submarines.First().Horizontal;
            var y = TestShip.Submarines.First().Vertical;
            var playerTurnId = PlayerTurnId;
            var playerToHitId = PlayerToHitId;
            _state.Shot(playerTurnId, x, y);
            var status = _state.GetStatus();

            status.ShotHistory.Should().HaveCount(1);
            var entry = status.ShotHistory.First();
            entry.PlayerId.Should().Be(playerTurnId);
            entry.PlayerToHitId.Should().Be(playerToHitId);
            entry.Horizontal.Should().Be(x);
            entry.Vertical.Should().Be(y);
            entry.IsKill.Should().BeTrue();
            entry.IsHit.Should().BeFalse();
            entry.IsMiss.Should().BeFalse();

            status.PlayerTurn.FieldStatus.TotalShips.Should().Be(TestShip.ShipsToFillField.Count() - 1);
        }

        [Fact]
        public void ShotWrongTurn()
        {
            var x = TestShip.BattleShip.Horizontal;
            var y = TestShip.BattleShip.Vertical;
            var playerToHitId = PlayerToHitId;

            _state.Invoking(s => s.Shot(playerToHitId, x, y)).Should().Throw<GameException>();
        }

        [Fact]
        public void EndGame()
        {
            KillAllShips(_otherPlayerId);

            var status = _state.GetStatus();

            status.Stage.Should().Be(GameStage.Finished);
            status.Winner.Should().NotBeNull();

            status.Winner.Id.Should().Be( _playerId);
        }

        private Guid PlayerToHitId => _state.GetStatus().PlayerToHit.Id;

        private Guid PlayerTurnId => _state.GetStatus().PlayerTurn.Id;

        private void KillAllShips(Guid playerToHit)
        {
            foreach(var ship in TestShip.ShipsToFillField)
            {
                foreach(var (x, y) in ship.GetShipSegmentsCoords())
                {
                    if (PlayerTurnId == playerToHit)
                    {
                        _state.Shot(playerToHit, 0, 8);
                    }
                    _state.Shot(PlayerTurnId, x, y);
                }
            }
        }
    }
}