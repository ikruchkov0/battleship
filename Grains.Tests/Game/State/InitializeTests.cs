using System;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using FluentAssertions;
using Grains.Game.State;
using Xunit;

namespace Grains.Tests.Game.State
{
    public class InitializeTests
    {
        private readonly GameState _state;

        public InitializeTests()
        {
            _state = new GameState();
        }

        [Fact]
        public void InitializePositive()
        {
            var gameId = Guid.NewGuid();
            _state.Initialize(gameId, "test");
            var status = _state.GetStatus();

            status.Id.Should().Be(gameId);
            status.PlayerTurn.Should().BeNull();
            status.Stage.Should().Be(GameStage.Created);
            status.Players.Should().BeEmpty();
        }

        [Fact]
        public void InitializeAlreadyCreated()
        {
            var gameId = Guid.NewGuid();
            _state.Initialize(gameId, "test");
            _state.Invoking(x => x.Initialize(gameId, "test")).Should().Throw<GameException>();
        }
    }
}