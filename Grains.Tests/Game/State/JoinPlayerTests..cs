using System;
using System.Linq;
using Battleship.Common.Enums;
using Battleship.Common.Exceptions;
using FluentAssertions;
using Grains.Game.State;
using Xunit;

namespace Grains.Tests.Game.State
{
    public class JoinPlayerTests
    {
        private readonly GameState _state;

        public JoinPlayerTests()
        {
            _state = new GameState();
            _state.Initialize(Guid.NewGuid(), "test");
        }

        [Fact]
        public void JoinPlayerPositive()
        {
            var playerId = Guid.NewGuid();
            _state.JoinPlayer(playerId);
            var status = _state.GetStatus();

            status.PlayerTurn.Should().BeNull();
            status.Players.Should().NotBeNull();
            status.Players.Should().NotBeEmpty();

            var player = status.Players.First(x => x.Id == playerId);
            player.FieldStatus.Should().NotBeNull();
            player.FieldStatus.TotalShips.Should().Be(0);

        }

        [Fact]
        public void JoinPlayerToFull()
        {
            var playerId1 = Guid.NewGuid();
            var playerId2 = Guid.NewGuid();
            _state.JoinPlayer(playerId1);
            _state.JoinPlayer(playerId2);
            var status = _state.GetStatus();

            status.Stage.Should().Be(GameStage.Created);

            status.Players.Should().NotBeNull();
            status.Players.Select(x => x.Id).Should().Contain( new [] { playerId1, playerId2});
        }

        [Fact]
        public void JoinPlayerMoreThanLimit()
        {
            var playerId1 = Guid.NewGuid();
            var playerId2 = Guid.NewGuid();
            var playerId3 = Guid.NewGuid();
            _state.JoinPlayer(playerId1);
            _state.JoinPlayer(playerId2);

            _state.Invoking(x => x.JoinPlayer(playerId3)).Should().Throw<GameException>();
        }

        [Fact]
        public void JoinPlayerAlreadyAdded()
        {
            var playerId1 = Guid.NewGuid();
            var playerId2 = Guid.NewGuid();
            _state.JoinPlayer(playerId1);
            _state.JoinPlayer(playerId2);

            _state.Invoking(x => x.JoinPlayer(playerId2)).Should().Throw<GameException>();
        }
    }
}